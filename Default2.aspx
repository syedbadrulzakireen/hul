﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
        .title
        {
            color: #f1efeb;
            font-weight: bold;
            font-size: 87px;
            margin-top: -105px;
        } 
        
        @media only screen and (max-width: 600px) {
        .title {
            color: white;
            font-weight: bold;
            font-size: 59px;
            margin-top: 110px;            
        }
}     
    </style>
    
<div class="main-slide ">
    <div class="main-slide__btn-item " ></div>
        <div id="sliderMain" class="main-slide__slider ">           
            <div class="main-slide__item-a ">
                <div data-bgimage="images/slider1.jpg" class="main-slide__img-wrap bg-image" style="left:0px; background-attachment:fixed;  position:absolute" ></div>
                 
                <div class="container">
                    <center >  <h1 class="title"> Welcome to Sheraton Grand</h1> </center> 
                    <div class="row">
                        <div class="b-tabs col-lg-8" style="width:99%;">
                            <h6 class="s6-heading b-tabs__ttl">Top events</h6>
                            <ul role="tablist" class="">
                                <li role="presentation" class="active">
                                    <a href="#block-a" aria-controls="block-a" role="tab" data-toggle="tab">
                                        <svg baseProfile="tiny" width="10" height="10" x="0px" y="0px" viewBox="0 0 10 10" xml:space="preserve">
                                            <path fill="#231F20" d="M0,4h4V0H0V4z M6,0v4h4V0H6z M0,10h4V6H0V10z M6,10h4V6H6V10z"/>
                                        </svg>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#block-b" aria-controls="block-b" role="tab" data-toggle="tab">
                                    <svg baseProfile="tiny" width="10" height="10" x="0px" y="0px" viewBox="0 0 10 10" xml:space="preserve">
                                        <path fill="#231F20" d="M0,0v4h10V0H0z M10,10V6H0v4H10z"/>
                                    </svg>
                                    </a>
                                </li>
                            </ul>

                              <div class="b-tabs__content">
                                <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">

                                  <div class="b-tabs__pane-item col-sm-6">
                                    <div class="c-box">
                                      <div class="c-box__inner">
                                        <div class="c-box__cont">
                                              <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                              <hr class="hr-pattern">
                                              <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>
                                        </div>
                                        <div data-bgimage="../uploads/events/EventsTiles_01_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <div class="c-box__date-item">
                                             <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>
                                        <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap">
                                                <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                                                <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                                          </div>
                                        </div><a href="#" rel="nofollow" class="b-box__link" style="background-color: #a54399;opacity: 0.5;"></a>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="b-tabs__pane-item col-sm-6">
                                    <div class="c-box">
                                      <div class="c-box__inner">
                                        <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                          <hr class="hr-pattern">
                                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>
                                        </div>
                                        <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <div class="c-box__date-item">
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>
                                        <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap">
                                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                                            <svg class="btn-pointer__left" x="0px" y="0px"
                                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                                          </div>
                                        </div><a href="event-open.html" rel="nofollow" class="b-box__link" style="background-color: #a54399;opacity: 0.5;"></a>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="b-tabs__pane-item col-sm-6">
                                    <div class="c-box">
                                      <div class="c-box__inner">
                                        <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                          <hr class="hr-pattern">
                                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>
                                        </div>
                                        <div data-bgimage="../uploads/events/EventsTiles_03_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <div class="c-box__date-item">
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>
                                        <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap">
                                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                                            <svg class="btn-pointer__left" x="0px" y="0px"
                                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                                          </div>
                                        </div><a href="event-open.html" rel="nofollow" class="b-box__link" style="background-color: #a54399;opacity: 0.5;"></a>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="b-tabs__pane-item col-sm-6">
                                    <div class="c-box">
                                      <div class="c-box__inner">
                                        <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                          <hr class="hr-pattern">
                                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>
                                        </div>
                                        <div data-bgimage="../uploads/events/EventsTiles_04_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <div class="c-box__date-item">
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>
                                        <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap">
                                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                                            <svg class="btn-pointer__left" x="0px" y="0px"
                                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                                          </div>
                                        </div><a href="event-open.html" rel="nofollow" class="b-box__link" style="background-color: #a54399;opacity: 0.5;"></a>
                                      </div>
                                    </div>
                                  </div>

                                  <%--<div class="b-box__btn-wrap txt--centr col-sm-12">
                                      <div class="btn-icon__wrap">
                                      <a href="#" class="btn btn-icon">More events</a>
                                      </div>
                                  </div>--%>
                                </div>
            
                              </div>
            
                          </div>
                    </div>
                </div>
            </div>
        </div>
</div> 


<%--<div class="b-events">
    
     <div class="container" ">
        <div class="row">
            <div class="b-tabs col-lg-8" style="width:99%;">
            <h6 class="s6-heading b-tabs__ttl">Top events</h6>
            <ul role="tablist" class="b-tabs__nav b-tabs__nav-tabs">
                <li role="presentation" class="active">
                    <a href="#block-a" aria-controls="block-a" role="tab" data-toggle="tab">
                        <svg baseProfile="tiny" width="10" height="10" x="0px" y="0px" viewBox="0 0 10 10" xml:space="preserve">
                            <path fill="#231F20" d="M0,4h4V0H0V4z M6,0v4h4V0H6z M0,10h4V6H0V10z M6,10h4V6H6V10z"/>
                        </svg>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#block-b" aria-controls="block-b" role="tab" data-toggle="tab">
                    <svg baseProfile="tiny" width="10" height="10" x="0px" y="0px" viewBox="0 0 10 10" xml:space="preserve">
                        <path fill="#231F20" d="M0,0v4h10V0H0z M10,10V6H0v4H10z"/>
                    </svg>
                    </a>
                </li>
            </ul>

              <div class="b-tabs__content">
                <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">

                  <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                              <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                              <hr class="hr-pattern">
                              <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>
                        </div>
                        <div data-bgimage="../uploads/events/EventsTiles_01_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                        <div class="c-box__date-item">
                             <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>
                        <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                                <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                                <svg class="btn-pointer__left" x="0px" y="0px"
                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="#" rel="nofollow" class="b-box__link" style="background-color: #a54399;"></a>
                      </div>
                    </div>
                  </div>

                  <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                          <hr class="hr-pattern">
                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>
                        </div>
                        <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                        <div class="c-box__date-item">
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>
                        <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="event-open.html" rel="nofollow" class="b-box__link" style="background-color: #a54399;"></a>
                      </div>
                    </div>
                  </div>

                  <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                          <hr class="hr-pattern">
                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>
                        </div>
                        <div data-bgimage="../uploads/events/EventsTiles_03_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                        <div class="c-box__date-item">
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>
                        <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="event-open.html" rel="nofollow" class="b-box__link" style="background-color: #a54399;"></a>
                      </div>
                    </div>
                  </div>

                  <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                          <hr class="hr-pattern">
                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>
                        </div>
                        <div data-bgimage="../uploads/events/EventsTiles_04_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                        <div class="c-box__date-item">
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>
                        <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="event-open.html" rel="nofollow" class="b-box__link" style="background-color: #a54399;"></a>
                      </div>
                    </div>
                  </div>

                  <div class="b-box__btn-wrap txt--centr col-sm-12">
                      <div class="btn-icon__wrap">
                      <a href="#" class="btn btn-icon">More events</a>
                      </div>
                  </div>
                </div>
            
              </div>
            
          </div>
        </div>
      </div>

</div>--%>
</asp:Content>

