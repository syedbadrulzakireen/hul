﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="highfame.aspx.cs" Inherits="highfame" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <!-- Sliding start -->
    <div class="main-slide">
        <div class="main-slide__btn-item"></div>
        <div id="sliderMain" class="main-slide__slider">
          <div class="main-slide__item-a">
            <div data-bgimage="images/high-dine.jpg" class="main-slide__img-wrap bg-image"></div>
            <div class="container" >
              
            </div>
          </div>
        
          <div class="main-slide__item-b" >
             
            <div class="main-slide__bg-wrap bg-grdnt"  style="background-image:url(images/high-mix.jpg)"></div>
            <div class="container">
              
            </div>
          </div>
        </div>
      </div>
     <!-- Sliding end -->
    
     <!-- high fame -->
    <style>
        img.style{
            width: 68%;
            height: 188px;
            border-bottom: solid;
            border-left: solid;
            border-right: solid;
            border-color: darkgoldenrod;
            border-style: dotted;
            border-radius: 114px;
        }
    </style>

    <div class="b-promo" style="background-color:black; border-top:solid;border-color:darkgoldenrod;">
        <div class="container">
          <h2 class="b-promo__s2-ttl" style="color:goldenrod;">‘HIGH FAME !'</h2>
            <br />
            <div class ="row" <%--style="border-style: dotted;border-color: white;"--%>>
                 <div class ="col-md-2"></div> 
                 <div class ="col-md-3">
                    <img src="images/High fame/Best Lounge Bar 2016.jpg" class ="style" />
   
                 </div>
                 <div class ="col-md-5" >
                <h2 style ="color :#fa2d21;"> Best Lounge Bar 2016</h2>
                <h4 style ="color :#ffffff;"> Best Lounge Bar in the city declared by Times</h4>
                 </div>
                <div class ="col-md-2"></div> 
           </div> 
            <div class ="row" <%--style="border-style: dotted;border-color: darkgoldenrod;"--%>>
                <div class ="col-md-2"></div>  
                <div class ="col-md-3">
                    <img src="images/High fame/Best Bartender 2015.png" class ="style" />
                 </div>
                 <div class ="col-md-5">
                <h2 style ="color :#fa2d21;"> Best Bartender 2015</h2>
                <h4 style ="color :#ffffff;"> Best Lounge Bar 2015 at the Times Food Award 2015.</h4>
                 </div>
                <div class ="col-md-2"></div> 
           </div> 
            <div class ="row" <%--style="border-style: dotted;border-color: white;"--%>>
                 <div class ="col-md-2"></div> 
                 <div class ="col-md-3" >
                    <img src="images/High fame/Sling Awards 2015.jpg" class ="style" />
                 </div>
                 <div class ="col-md-5" >
                <h2 style ="color :#fa2d21;"> Sling Awards 2015</h2>
                <h4 style ="color :#ffffff;"> Selected amongst the top five across the globe at The Peter . F . Heering Sling Awards 2015</h4>
                 </div>
                 <div class ="col-md-2"></div> 
           </div> 
            <div class ="row" <%--style="border-style: dotted;border-color: darkgoldenrod;"--%>>
                 <div class ="col-md-2"></div> 
                 <div class ="col-md-3">
                    <img src="images/High fame/Best Social Media Strategy.jpg" class ="style" />
                 </div>
                 <div class ="col-md-5">
                <h2 style ="color :#fa2d21;"> Best Social Media Strategy</h2>
                <h4 style ="color :#ffffff;"> Best Lounge Bar (Bangalorel at the Times Food and Nightlife Awards, 2015)</h4>
                 <div class ="col-md-2"></div> 
                 </div>
           </div> 
            <div class ="row" <%--style="border-style: dotted;border-color: white;"--%>>
                 <div class ="col-md-2"></div> 
                 <div class ="col-md-3">
                    <img src="images/High fame/Best Regional Standalone Restaurant.jpg" class ="style"/>
                 </div>
                 <div class ="col-md-5">
                <h2 style ="color :#fa2d21;"> Best Regional Standalone Restaurant</h2>
                <h4 style ="color :#ffffff;"> Best Regional Standalone Restaurant of the Year 2014 by lndian Restaurant Congress.</h4>
                 </div>
                 <div class ="col-md-2"></div> 
           </div> 
            <div class ="row" <%--style="border-style: dotted;border-color: darkgoldenrod;"--%>>
                 <div class ="col-md-2"></div> 
                 <div class ="col-md-3">
                    <img src="images/High fame/Best Regional Debutant Restaurant.jpg" class ="style"/>
                 </div>
                 <div class ="col-md-5">
                <h2 style ="color :#fa2d21;"> Best Regional Debutant Restaurant</h2>
                <h4 style ="color :#ffffff;"> Best Regional Debutant Restaurant of the Year 2014 by Indian Restaurant Congress.</h4>
                 </div>
                 <div class ="col-md-2"></div> 
           </div> 
          
        </div>
      </div>
    <!-- high fame end-->
</asp:Content>

