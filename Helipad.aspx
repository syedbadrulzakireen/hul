﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Helipad.aspx.cs" Inherits="Helipad" %>--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Helipad.aspx.cs" Inherits="Helipad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                 <style type="text/css">
         @import url("https://fast.fonts.net/lt/1.css?apiType=css&c=8d5e1514-dd0b-46fe-8111-004f45677b51&fontids=1476004");
         @font-face {
         font-family: "Univers LT W01_57 Condensed";
         src: url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix");
         src: url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix") format("eot"),url("Fonts/1476004/bf8f8741-5f64-4db9-a877-a44624092e68.woff2") format("woff2"),url("Fonts/1476004/7ce02c2c-45d4-4dee-90f2-f8034e29ac24.woff") format("woff"),url("Fonts/1476004/0955c906-88fc-47e8-8ea2-0765bdf88050.ttf") format("truetype");
		 }</style>
    <!-- Sliding start -->

    <%-- <div class="b-headlines"style="background-image:url(images/ImagesUpdated/Brunch-in-Ottawa-650x420.jpg);">
        <div class="container" >
          <div class="b-breadcrumbs">
           
          </div>
          
        </div>
      </div>--%>
    <!-- Sliding end -->
    <style>
        .b-panel-t::before {
            /*border-left: 0px solid #530d85;*/
        }

        .container {
            width: 1100px;
        }

        #a1 {
            width: 50px;
            float: left;
        }
        .c-box__inner .bg-image::before {    background-color: rgba(0, 0, 0, 0) !important;
        }

        @media screen and (max-width: 960px) {
            .container {
                width: 100%;
            }

            #a1 {
                width: 20%;
            }
        }

        @media screen and (max-width:640px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:250px!important;
                 background-repeat:no-repeat!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
            }
            .mobimMargin{
             
                 margin-right:12px!important;
              
            }
           #a1{top: 57px!important;
    margin-bottom: -4px!important;

           }
        }
         @media screen and (max-width:960px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:250px!important;
                background-repeat:no-repeat!important;

            }
            .ctext{
                width:100% !important;
                 height:auto!important;
            }
            .mobimMargin{
             
                 margin-right:12px!important;
              
            }
        }
          @media screen and (max-width:1200px) {
           .mobimMargin{
             
                 margin-right:12px!important;
              
            }
    </style>

    <div class="b-events" style="background-image: url(images/slider5.jpg); background-attachment: fixed;background-size:cover!important">
        <div class="container">
        

                    <div class="b-content container ">

                        <div id="a1" class="b-panel-t col-xs-4" style="top: -4px; margin-bottom: -66px; opacity: 0.9; width: 255px; height: 5px; /*margin-left: 11.5%; */ border-left: 0px solid #202020;">
                            <h2 style="color: white; text-align: justify; font-family: 'Univers LT W01_57 Condensed' !important; margin-top: -6px;">THE HELIPAD</h2>
                         
                        </div>
                    </div>



             

                            <div id="a3" class="b-tabs__pane-item col-lg-6 col-sm-12 col-md-6 col-xs-12" style="/*margin-top: 428px; */margin-bottom: 0px;margin-left:0px; ">
                                <%--   <div class="c-box">--%>
                                <div class="c-box__inner ctext" style="height: 750px; width:824px; margin-bottom: 0px; cursor:auto!important">
                                    <div class="c-box__cont" style="width:100%!important"> 
                                        <div id="a2" data-bgimage="images/ImagesUpdated/Helipad.jpg" class="b-box__img-wrap mobims bg-image col-sm-12" style="height:414px; width: 760px; margin-left: 5px; margin-bottom: 20px;  "></div>
                                       
                                        <h3 class="c-box__ttl-s3" style="font-family: 'Univers LT W01_57 Condensed' !important">THE HELIPAD</h3>
                                         <p class="c-box__txt" style="font-family: 'Univers LT W01_57 Condensed' !important; text-align: justify; font-size: 20px;">
                                           Get exclusive access to one of Bengaluru’s highest destinations at the World Trade Centre Helipad @ 421 feet.

 

Capture breathtaking special moments, propose to your loved one or click the perfect selfie <br /> Let your imagination soar at the Helipad from the highest dining destination in the South India.

 <br /><br />

#GetHIGHatBLR
                                        </p>
                                    </div>
                                   
                                   
                                    <a rel="nofollow" class="b-box__link mobimMargin" style="background-color: #202020; /*border: 1px solid #e15e32;*/ opacity: 0.7;"></a>
                             
                                </div>
                            </div>




                          
                      <%--  </div>--%>



                <%--    </div>--%>
            </div>
            <div class="container"> 
          <div class="row  ">
                <div id=" a4" class="btn-pointer-b__wrap " style="text-align: center; margin-left: 32px; margin-top: 11px;">
                    <a href="contacthigh.aspx" class="btn-pointer-b btn-anim-b"><b>Enquire Now</b></a>
                    <svg class="left" x="0px" y="0px" viewbox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>

                    <svg class="right" x="0px" y="0px" viewbox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>

                </div>

      



             <%--   <div class="btn-pointer-b__wrap "  style="text-align: center; margin-left: 15px; margin-top: 100px; /*margin-left: 14px;*/">
                    <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" class="btn-pointer-b btn-anim-b"><b>Book Now</b></a>
                    <svg class="left" x="0px" y="0px" viewbox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z" />
            </svg>

                    <svg class="right" x="0px" y="0px" viewbox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>

                </div>--%>
       
                </div>
        </div>
        </div>
    
   

</asp:Content>


