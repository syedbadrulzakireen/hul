﻿using Hul_login.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hul_login
{
    public partial class AdminDashBoard1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            
        }

        public void SortButton_Click(object sender, EventArgs e)
        {
            string expression = "";
            SortDirection direction;

            expression = SortList1.SelectedValue; /*+ "," + SortList2.SelectedValue;*/
            switch (DirectionList.SelectedValue)
            {
                case "Ascending":
                    direction = SortDirection.Ascending;
                    break;
                case "Descending":
                    direction = SortDirection.Descending;
                    break;
                default:
                    direction = SortDirection.Ascending;
                    break;
            }

            // Use the Sort method to programmatically sort the GridView
            // control using the sort expression and direction.
            GridView1.Sort(expression, direction);


        }

        private void ExportGridToExcel()
        {
            GridView g = new GridView();
            g = GridView1;
            g.Columns.RemoveAt(0);

            //GridView1.DataBind();
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "HUL" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            g.GridLines = GridLines.Both;
            g.HeaderStyle.Font.Bold = true;

            g.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();



        }

        protected void BtnExcel_Click(object sender, EventArgs e)
        {
           
            ExportGridToExcel();
        }

        protected void BtnExcel_Click1(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }
    }
}