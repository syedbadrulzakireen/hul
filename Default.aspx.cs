﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hul_login
{
    public partial class _Default : Page
    
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("User"))
            {
              //  Response.Redirect("/Default.aspx", true);
            }
            else if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                Response.Redirect("/AdminDashboard1.aspx", true);
            }
            else
            {

            }
        }
    }
}