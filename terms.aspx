﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="terms.aspx.cs" Inherits="terms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- Sliding start -->

  

 <div class="b-headlines"style="background-image:url(images/high-edge.jpg);height:350px;">
        <div class="container" >
          <div class="b-breadcrumbs">
            <ul class="b-breadcrumbs__list x-small-txt">
                <br />
                <br />
                <br />
              <li><a href="Default.aspx" class="link">main</a></li>
              <li style="color:white;">Terms of Service</li>
            </ul>
          </div>
            <br />
            <br />
          <h1 class="b-headlines__ttl" style="color:white;">Terms of Service</h1>
        </div>
      </div>
       
   
     <!-- Sliding end -->

    <!--drinksanddine-->
<div class="wrap" style="background-color: black;" >
    <div class="container"style="background-color:#0f0f0f;">
						<section class="col-sm-12">
                            <a id="main-content"></a>
					<h1 class="page-header"style="color:white;">Terms of service</h1>
						  						  						  						  													  						  						  						    <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <article id="node-140" class="node node-page clearfix" about="/terms-of-service" typeof="foaf:Document">
    <header >
            <span property="dc:title" content="Terms of service" class="rdf-meta element-hidden"></span>
        <span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>      

    </header>
    <div class="field field-name-body field-type-text-with-summary field-label-hidden">
        <div class="field-items">
            <div class="field-item even" property="content:encoded">
                <div class="terms-service-block"style="color:white; font-family:'Univers LT W01_57 Condensed' !important;   ">
<p ><strong>1. Acceptance of terms of service and amendments.</strong></p>
<p>Each time you use or cause access to this web site, you agree to be bound by these Terms of service, as amended from time to time with or without notice to you. In addition, if you are using a particular service on this web site or accessed via this web site, you will be subject to any rules or guidelines applicable to those services, and they will be incorporated by reference within these Terms of service. Please read the site’s Privacy policy, which is incorporated within these Terms of service by reference.</p>
<p><strong>2. The site editor’s service</strong></p>
<p>This web site and the services provided to you on and via this web site are provided on an “AS IS” basis. You agree that the site editor reserves the right to modify or discontinue provision of this web site and its services, and to remove the data you provide, either temporarily or permanently, at any time, without notice and without any liability towards you, The site editor will not be held responsible or liable for timeliness, removal of information, failure to store information, inaccuracy of information, or improper delivery of information.</p>
<p><strong>3. Your responsibilities and registration obligations</strong></p>
<p>In order to use this web site or certain parts of it, you may be required to register for a user account on this web site; in this case, you agree to provide truthful information when requested, and — if a minimum age is required for eligibility for a user account — you undertake that you are at least the required age. By registering for a user account, you explicitly agree to this site’s Terms of service, including any amendments made by the site editor that are published herein.</p>
<p><strong>4. Privacy policy</strong></p>
<p>Registration data and other personally identifiable information that the site may collect is subject to the terms of the site editor’s Privacy policy.</p>
<p><strong>5. Registration and password</strong></p>
<p>You are responsible for maintaining the confidentiality of your password, and you will be responsible for all usage of your user account and/or user name, whether authorized or not authorized by you. You agree to immediately notify the site editor of any unauthorized use of your user account, user name or password.</p>
<p><strong>6. Your conduct</strong></p>
<p>You agree that all information or data of any kind, whether text, software, code, music or sound, photographs or graphics, video or other materials (“content”), made available publicly or privately, will be under the sole responsibility of the person providing the said content, or of the person whose user account is used. You agree that this web site may expose you to content that may be objectionable or offensive. The site editor will not be responsible to you in any way for content displayed on this web site, nor for any error or omission.</p>
<p><strong>By using this web site or any service provided, you explicitly agree that:</strong></p>
<p>(a) you will not provide any content or conduct yourself in any way that may be construed as: unlawful; illegal; threatening; harmful; abusive; harassing; stalking; tortious; defamatory; libelous; vulgar; obscene; offensive; objectionable; pornographic; designed to interfere with or disrupt the operation of this web site or any service provided; infected with a virus or other destructive or deleterious programming routine; giving rise to civil or criminal liability; or in violation of an applicable local, national or international law;</p>
<p>(b) you will not impersonate or misrepresent your association with any person or entity; you will not forge or otherwise seek to conceal or misrepresent the origin of any content provided by you;</p>
<p>(c) you will not collect or harvest any information about other users;</p>
<p>(d) you will not provide, and you will not use this web site to provide, any content or service in any commercial manner, or in any manner that would involve junk mail, spam, chain letters, pyramid schemes, or any other form of unauthorized advertising or commerce; you will not use this web site to promote or operate any service or content without the site editor’s prior written consent;</p>
<p>(e) you will not provide any content that may give rise to the site editor being held civilly or criminally liable, or that may be considered a violation of any local, national or international law, including — but not limited to — laws relating to copyrights, trademarks, patents, or trade secrets.</p>
<p>7. Submission of content on this web site</p>
<p><strong>By providing any content to this web site:</strong></p>
<p>(a) you agree to grant the site editor a worldwide, royalty-free, perpetual, non-exclusive right and license (including any moral rights or other necessary rights.) to use, display, reproduce, modify, adapt, publish, distribute, perform, promote, archive, translate, and to create derivative works and compilations, in whole or in part. Such license will apply with respect to any form, media, technology already known at the time of provision or developed subsequently;</p>
<p>(b) you warrant and represent that you have all legal, moral, and other rights that may be necessary to grant the site editor the license specified in this section 7;</p>
<p>(c) you acknowledge and agree that the site editor will have the right (but not obligation), at the site editor’s entire discretion, to refuse to publish, or to remove, or to block access to any content you provide, at any time and for any reason, with or without notice.</p>
<p><strong>8. Third-party services</strong></p>
<p>Goods and services of third parties may be advertised and/or may be made available on or through this web site. Representations made regarding products and services provided by third parties will be governed by the policies and representations made by these third parties. The site editor will not in any manner be liable for or responsible for any of your dealings or interaction with third parties.</p>
<p><strong>9. Indemnification</strong></p>
<p>You agree to indemnify and hold harmless the site editor and the site editor’s representatives, subsidiaries, affiliates, related parties, officers, directors, employees, agents, independent contractors, advertisers, partners, and co-branders, from any claim or demand, including reasonable legal fees, that may be filed by any third party, arising out of your conduct or connection with this web site or service, your provision of content, your violation of these Terms of service, or any other violation by you of the rights of another person or party.</p>
<p><strong>10. Disclaimer of warranties</strong></p>
<p>You understand and agree that your use of this web site and of any services or content provided (the “service”) is at your own risk. Services and content are provided to you “as is”, and the site editor expressly disclaims all warranties of any kind, either implied or express, including but not limited to warranties of merchantability, fitness for a particular purpose, and non-infringement.</p>
<p>The site editor makes no warranty, either implied or express, that any part of the service will be uninterrupted, error-free, virus-free, timely, secure, accurate, reliable, or of any quality, nor is it warranted either implicitly or expressly that any content is safe in any manner for download. You understand and agree that neither the site editor nor any participant in the service provides professional advice of any kind and that any advice or any other information obtained via this web site may be used solely at your own risk, and that the site editor will not be held liable in any way.</p>
<p>Some jurisdictions may not allow disclaimers of implied warranties, and certain statements in the above disclaimer may not apply to you as regards implied warranties; the other terms and conditions remain enforceable notwithstanding.</p>
<p><strong>11. Limitation of liability</strong></p>
<p>You expressly understand and agree that the site editor will not be liable for any direct, indirect, special, incidental, consequential or exemplary damages; this includes, but is not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if the site editor has been advised of the possibility of such damages), resulting from (i) the use of services or the inability to use services, (ii) the cost of obtaining substitute goods and/or services resulting from any transaction entered into on through services, (iii) unauthorized access to or alteration of your data transmissions, (iv) statements by any third party or conduct of any third party using services, or (v) any other matter relating to services.</p>
<p>In some jurisdictions, it is not permitted to limit liability and, therefore, such limitations may not apply to you.</p>
<p><strong>12. Reservation of rights</strong></p>
<p>The site editor reserves all of the site editor’s rights, including but not limited to any and all copyrights, trademarks, patents, trade secrets, and any other proprietary right that the site editor may have in respect of this web site, its content, and goods and services that may be provided. The use of the site editor’s rights. and property requires the site editor’s prior written consent. By making services available to you, the site editor is not providing you with any implied or express licenses or rights, and you will have no rights to make any commercial use of this web site or provided services without the site editor’s prior written consent.</p>
<p><strong>13. Notification of copyright infringement</strong></p>
<p>If you believe that your property has been used in any way that could be considered a copyright infringement or a violation of your intellectual property rights, the site editor\’s copyright agent may be contacted via:</p>
<p>e-mail to the site administrator</p>
<p>[Other contact information, if published on the site:]</p>
<p><strong>14. Applicable law</strong></p>
<p>You agree that these Terms of service and any dispute arising out of your use of this web site or products or services provided will be governed by and construed in accordance with local laws applicable at the site editor’s domicile, notwithstanding any differences between the said applicable legislation and legislation in force at your location. By registering for a user account on this web site, or by using this web site and the services it provides, you accept that jurisdiction is granted to the courts having jurisdiction over the site editor’s domicile, and that any disputes will be heard by the said courts.</p>
<p><strong>15. Miscellaneous information</strong></p>
<p>(i) In the event that any provision of these Terms of service is deemed to conflict with legislation by a court with jurisdiction over the parties, the said provision will be interpreted to reflect the original intentions of the parties in accordance with applicable law, and the remainder of these Terms of service will remain valid and applicable; (ii) The failure of either party to assert any right under these Terms of service will not be considered to be a waiver of that party’s right, and the said right will remain in full force and effect; (iii) You agree that any claim or cause in respect of this web site or its services must be filed within one (1) year after such claim or cause arose, or the said claim or cause will be forever barred, without regard to any contrary legislation; (iv) The site editor may assign the site editor’s rights and obligations under these Terms of service; in this event, the site editor will be relieved of any further obligation.</p>
<p><strong>1. Acceptance of terms of service and amendments.</strong></p>
<p>Each time you use or cause access to this web site, you agree to be bound by these Terms of service, as amended from time to time with or without notice to you. In addition, if you are using a particular service on this web site or accessed via this web site, you will be subject to any rules or guidelines applicable to those services, and they will be incorporated by reference within these Terms of service. Please read the site’s Privacy policy, which is incorporated within these Terms of service by reference.</p>
<p><strong>2. The site editor’s service</strong></p>
<p>This web site and the services provided to you on and via this web site are provided on an “AS IS” basis. You agree that the site editor reserves the right to modify or discontinue provision of this web site and its services, and to remove the data you provide, either temporarily or permanently, at any time, without notice and without any liability towards you, The site editor will not be held responsible or liable for timeliness, removal of information, failure to store information, inaccuracy of information, or improper delivery of information.</p>
<p><strong>3. Your responsibilities and registration obligations</strong></p>
<p>In order to use this web site or certain parts of it, you may be required to register for a user account on this web site; in this case, you agree to provide truthful information when requested, and — if a minimum age is required for eligibility for a user account — you undertake that you are at least the required age. By registering for a user account, you explicitly agree to this site’s Terms of service, including any amendments made by the site editor that are published herein.</p>
<p><strong>4. Privacy policy</strong></p>
<p>Registration data and other personally identifiable information that the site may collect is subject to the terms of the site editor’s Privacy policy.</p>
<p><strong>5. Registration and password</strong></p>
<p>You are responsible for maintaining the confidentiality of your password, and you will be responsible for all usage of your user account and/or user name, whether authorized or not authorized by you. You agree to immediately notify the site editor of any unauthorized use of your user account, user name or password.</p>
<p><strong>6. Your conduct</strong></p>
<p>You agree that all information or data of any kind, whether text, software, code, music or sound, photographs or graphics, video or other materials (“content”), made available publicly or privately, will be under the sole responsibility of the person providing the said content, or of the person whose user account is used. You agree that this web site may expose you to content that may be objectionable or offensive. The site editor will not be responsible to you in any way for content displayed on this web site, nor for any error or omission.</p>
<p><strong>By using this web site or any service provided, you explicitly agree that:</strong></p>
<p>(a) you will not provide any content or conduct yourself in any way that may be construed as: unlawful; illegal; threatening; harmful; abusive; harassing; stalking; tortious; defamatory; libelous; vulgar; obscene; offensive; objectionable; pornographic; designed to interfere with or disrupt the operation of this web site or any service provided; infected with a virus or other destructive or deleterious programming routine; giving rise to civil or criminal liability; or in violation of an applicable local, national or international law;</p>
<p>(b) you will not impersonate or misrepresent your association with any person or entity; you will not forge or otherwise seek to conceal or misrepresent the origin of any content provided by you;</p>
<p>(c) you will not collect or harvest any information about other users;</p>
<p>(d) you will not provide, and you will not use this web site to provide, any content or service in any commercial manner, or in any manner that would involve junk mail, spam, chain letters, pyramid schemes, or any other form of unauthorized advertising or commerce; you will not use this web site to promote or operate any service or content without the site editor’s prior written consent;</p>
<p>(e) you will not provide any content that may give rise to the site editor being held civilly or criminally liable, or that may be considered a violation of any local, national or international law, including — but not limited to — laws relating to copyrights, trademarks, patents, or trade secrets.</p>
<p>7. Submission of content on this web site</p>
<p><strong>By providing any content to this web site:</strong></p>
<p>(a) you agree to grant the site editor a worldwide, royalty-free, perpetual, non-exclusive right and license (including any moral rights or other necessary rights.) to use, display, reproduce, modify, adapt, publish, distribute, perform, promote, archive, translate, and to create derivative works and compilations, in whole or in part. Such license will apply with respect to any form, media, technology already known at the time of provision or developed subsequently;</p>
<p>(b) you warrant and represent that you have all legal, moral, and other rights that may be necessary to grant the site editor the license specified in this section 7;</p>
<p>(c) you acknowledge and agree that the site editor will have the right (but not obligation), at the site editor’s entire discretion, to refuse to publish, or to remove, or to block access to any content you provide, at any time and for any reason, with or without notice.</p>
<p><strong>8. Third-party services</strong></p>
<p>Goods and services of third parties may be advertised and/or may be made available on or through this web site. Representations made regarding products and services provided by third parties will be governed by the policies and representations made by these third parties. The site editor will not in any manner be liable for or responsible for any of your dealings or interaction with third parties.</p>
<p><strong>9. Indemnification</strong></p>
<p>You agree to indemnify and hold harmless the site editor and the site editor’s representatives, subsidiaries, affiliates, related parties, officers, directors, employees, agents, independent contractors, advertisers, partners, and co-branders, from any claim or demand, including reasonable legal fees, that may be filed by any third party, arising out of your conduct or connection with this web site or service, your provision of content, your violation of these Terms of service, or any other violation by you of the rights of another person or party.</p>
<p><strong>10. Disclaimer of warranties</strong></p>
<p>You understand and agree that your use of this web site and of any services or content provided (the “service”) is at your own risk. Services and content are provided to you “as is”, and the site editor expressly disclaims all warranties of any kind, either implied or express, including but not limited to warranties of merchantability, fitness for a particular purpose, and non-infringement.</p>
<p>The site editor makes no warranty, either implied or express, that any part of the service will be uninterrupted, error-free, virus-free, timely, secure, accurate, reliable, or of any quality, nor is it warranted either implicitly or expressly that any content is safe in any manner for download. You understand and agree that neither the site editor nor any participant in the service provides professional advice of any kind and that any advice or any other information obtained via this web site may be used solely at your own risk, and that the site editor will not be held liable in any way.</p>
<p>Some jurisdictions may not allow disclaimers of implied warranties, and certain statements in the above disclaimer may not apply to you as regards implied warranties; the other terms and conditions remain enforceable notwithstanding.</p>
<p><strong>11. Limitation of liability</strong></p>
<p>You expressly understand and agree that the site editor will not be liable for any direct, indirect, special, incidental, consequential or exemplary damages; this includes, but is not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if the site editor has been advised of the possibility of such damages), resulting from (i) the use of services or the inability to use services, (ii) the cost of obtaining substitute goods and/or services resulting from any transaction entered into on through services, (iii) unauthorized access to or alteration of your data transmissions, (iv) statements by any third party or conduct of any third party using services, or (v) any other matter relating to services.</p>
<p>In some jurisdictions, it is not permitted to limit liability and, therefore, such limitations may not apply to you.</p>
<p><strong>12. Reservation of rights</strong></p>
<p>The site editor reserves all of the site editor’s rights, including but not limited to any and all copyrights, trademarks, patents, trade secrets, and any other proprietary right that the site editor may have in respect of this web site, its content, and goods and services that may be provided. The use of the site editor’s rights. and property requires the site editor’s prior written consent. By making services available to you, the site editor is not providing you with any implied or express licenses or rights, and you will have no rights to make any commercial use of this web site or provided services without the site editor’s prior written consent.</p>
<p><strong>13. Notification of copyright infringement</strong></p>
<p>If you believe that your property has been used in any way that could be considered a copyright infringement or a violation of your intellectual property rights, the site editor\’s copyright agent may be contacted via:</p>
<p>e-mail to the site administrator</p>
<p>[Other contact information, if published on the site:]</p>
<p><strong>14. Applicable law</strong></p>
<p>You agree that these Terms of service and any dispute arising out of your use of this web site or products or services provided will be governed by and construed in accordance with local laws applicable at the site editor’s domicile, notwithstanding any differences between the said applicable legislation and legislation in force at your location. By registering for a user account on this web site, or by using this web site and the services it provides, you accept that jurisdiction is granted to the courts having jurisdiction over the site editor’s domicile, and that any disputes will be heard by the said courts.</p>
<p><strong>15. Miscellaneous information</strong></p>
<p>(i) In the event that any provision of these Terms of service is deemed to conflict with legislation by a court with jurisdiction over the parties, the said provision will be interpreted to reflect the original intentions of the parties in accordance with applicable law, and the remainder of these Terms of service will remain valid and applicable; (ii) The failure of either party to assert any right under these Terms of service will not be considered to be a waiver of that party’s right, and the said right will remain in full force and effect; (iii) You agree that any claim or cause in respect of this web site or its services must be filed within one (1) year after such claim or cause arose, or the said claim or cause will be forever barred, without regard to any contrary legislation; (iv) The site editor may assign the site editor’s rights and obligations under these Terms of service; in this event, the site editor will be relieved of any further obligation.</p></div>
</div></div></div>    <footer>
          </footer>
    </article>

</section>
  </div>
						</section>

													
					</div>
					
				</div>
   <!-- google map -->
  <!--  <div class="b-promo" style="border-top:solid;border-color:darkgoldenrod;">
        <div style="width: 100%"><iframe width="100%" height="600" 
             src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=No.%2026%2F1%2C%20Rooftop%2C%20World%20Trade%20Center%2C%20Bangalore%20Brigade%20Gateway%20Campus%2C%20Dr.%20Rajkumar%20Road%2C%20Malleswaram%2C%20Bengaluru%2C%20Karnataka%20560055+(High%20Ultra%20Lounge)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" 
             frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">
                 Google Maps iframe generator</a></iframe>

        </div>
          
    </div> -->
	<div   id="bottom_link" style="width: 100%" ><iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=No.%2026%2F1%2C%20Rooftop%2C%20World%20Trade%20Center%2C%20Bangalore%20Brigade%20Gateway%20Campus%2C%20Dr.%20Rajkumar%20Road%2C%20Malleswaram%2C%20Bengaluru%2C%20Karnataka%20560055+(high%20ultra%20lounge)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Add map to website</a></iframe></div>
    <!-- google map -->
    
    <!--drinksanddine-->
</asp:Content>

