﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="eventshigh.aspx.cs" Inherits="eventshigh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <%--<!-- Sliding start -->
    <div class="main-slide" >
        <div class="main-slide__btn-item" ></div>
        <div id="sliderMain" class="main-slide__slider">
          <div class="main-slide__item-a" >
            <div data-bgimage="images/high-view.jpg" class="main-slide__img-wrap bg-image"></div>
           
          </div>
        
          <div class="main-slide__item-b" >
             
            <div class="main-slide__bg-wrap bg-grdnt"  style="background-image:url(images/high-mix.jpg);"></div>
         
          </div>
        </div>
      </div>
     <!-- Sliding end -->--%>
      <div class="c-headlines">
        <div data-stellar-background-ratio="1.2" data-bgimage="images/super-high11.jpg" class="c-headlines__img-wrap parallax bg-image"></div>
        <div class="container">
          <div class="b-breadcrumbs b-breadcrumbs--wht">
           <!--  <ul class="b-breadcrumbs__list x-small-txt">
            <li><a href="Default.aspx" class="link">main</a></li>
              <li><a href="eventshigh.aspx" class="link">events</a></li>
              <li>Book Now</li>
               <li><a href="eventshigh.aspx" class="link">Book Now</a></li>
            </ul>-->
          </div>
<%--          <div class="c-headlines__ttl">FRIENDS Private House Party Private Private House Party House Party</div>--%>
            
                <div class="col-12>"
       
            
          <div class="c-headlines__date-lg" style="right:900px;padding-top:65px; ">
            <time datetime="2015-09-23T08:00" class="date-lg" style="width:260px;"><span class="date-lg__dt" style="font-size:40px;">22-29</span><span class="date-lg__rh"><span class="date-lg__rh-m" ></span><span class="date-lg__rh-m">June 2018</span><span class="date-lg__rh-t"></span></span><span class="date-lg__ad" ><a href="#Friday">Fri</a></span></time>
          </div>>
            <div class="c-headlines__date-lg" style="right:600px;padding-top:50px; ">
            <time datetime="2015-09-23T08:00" class="date-lg" style="width:260px;"><span class="date-lg__dt"style="font-size:40px;">23-30</span><span class="date-lg__rh"><span class="date-lg__rh-m" ></span><span class="date-lg__rh-m">June 2018</span><span class="date-lg__rh-t"></span></span><span class="date-lg__ad" ><a href="#Saturday">Sat</a></span></time>
          </div>
                 <div class="c-headlines__date-lg" style="right:300px;padding-top:50px; ">
            <time datetime="2015-09-23T08:00" class="date-lg" style="width:260px;"><span class="date-lg__dt"style="font-size:40px;">--</span><span class="date-lg__rh"><span class="date-lg__rh-m" ></span><span class="date-lg__rh-m">June 2018</span><span class="date-lg__rh-t"></span></span><span class="date-lg__ad" ><a href="#Sunday">Sun</a></span></time>
          </div>
                 <div class="c-headlines__date-lg" style="right:0px;padding-top:50px; ">
            <time datetime="2015-09-23T08:00" class="date-lg" style="width:260px;"><span class="date-lg__dt"style="font-size:40px;">--</span><span class="date-lg__rh"><span class="date-lg__rh-m" ></span><span class="date-lg__rh-m">June 2018</span><span class="date-lg__rh-t"></span></span><span class="date-lg__ad" ><a href="#Others">Others</a></span></time>
          </div>
                
                    </div>
               

        </div>
      </div> 
      <div class="b-content container ">
        <div class="b-panel-t col-xs-12">
         <%-- <ul class="b-panel-t__list b-panel-t__arts col-sm-8 col-md-9">
            <li>
              <p>Andrew Feeling</p><span>Donetsk</span>
            </li>
            <li>
              <p>Shalim</p><span>Donetsk</span>
            </li>
            <li>
              <p>Stay B</p><span>Donetsk</span>
            </li>
            <li>
              <p>Someone Else</p><span>Mariupol</span>
            </li>
            <li>
              <p>Dranga</p><span>Mariupol</span>
            </li>
            <li>
              <p>Andrew Feeling</p><span>Donetsk</span>
            </li>
            <li>
              <p>Shalim</p><span>Donetsk</span>
            </li>
            <li>
              <p>Stay B</p><span>Donetsk</span>
            </li>
          </ul>--%><div class="b-panel-t__list col-md-12 col-md-3 ">
    <p  style="color:white;text-align:inherit;">
         
            <h2 style="color:white; text-align:justify;font-family:arial,Arial, Helvetica, sans-serif" "><b>I went on a week day. I experienced it to be really awesome experience but it wasn' though it was really nice. Food, service and ambience were good. It's worth visiting and can go multiple times as well.</b></h2>
         <h2 style="color:brown; text-align:center;font-family:arial,Arial, Helvetica, sans-serif" "><b>Review in Zomato By Pradeep. </b></h2>
         </p>



</div>

         <%-- <ul class="b-panel-t__list col-sm-4 col-md-3">
            <li><a href="mailto:tickets@cumeclub.com" class="link">high@sheraton.com</a></li>
            <li class="tel"><a href="tel:+380969776011" class="link">9180 4567 4567 </a></li>
          </ul>--%>
        </div>

</div>

    <!--Friday Events start-->
    <div class="b-gallery">
        <div data-bgimage="" class="b-gallery__img-wrap bg-image"></div>
        <div class="container">
          <div class="row">
            <div class="b-item__header col-xs-12">
              <h6 class="s6-heading b-item__ttl-header" id="Friday">Friday</h6>
            </div>
            <div id="sliderGallery" class="b-gallery__slider btn-white">
              <article class="c-box c-box--sm">
                <div class="c-box__inner"   style="background-color:purple; opacity:0.8;" >
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float" >
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">22</span><span class="date-sm__rh"><span class="date-sm__rh-m">June</span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></li>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <div id="headingIconClrOne" role="tab" class="b-accordion__panel-heading">
                        <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" href="#blockIconClrOne" aria-expanded="true" aria-controls="blockIconClrOne" style="color:white;"><b class="fa fa-music"style="color:white;"></b> <B>DJ NIGHT </B><b class="fa fa-angle-down"></b></a></h4>
                      </div>

                      <div id="blockIconClrOne" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                        <div class="b-accordion__body">
                          <p>
                            DJ:DJ Jasmeet</p>

                           <p>Period:<p>22<sup>nd</sup> June, 2018</p> </p>

                            <p>Time: 9:00 PM Onwards</p>
                        </div>
                      </div>
                
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="" class=" b-box__img-wrap b-box__grdnt-b bg-image" style="background-color:purple; opacity:0.5;"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">29</span><span class="date-sm__rh"><span class="date-sm__rh-m">June</span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                   <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                 <div id="headingIconClrtwo" role="tab" class="b-accordion__panel-heading">
                        <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" href="#blockIconClrtwo" aria-expanded="true" aria-controls="blockIconClrtwo" style="color:white;"><b class="fa fa-music"style="color:white;"></b> <B>DJ NIGHT </B><b class="fa fa-angle-down"></b></a></h4>
                      </div>
                      <div id="blockIconClrtwo" role="tabpanel" aria-labelledby="headingIconClrtwo" class="b-accordion__collapse collapse in">
                        <div class="b-accordion__body">
                           <p>
                            DJ: DJ Martin Dsouza</p>

                           <p>Period: <p>29<sup>th</sup> June, 2018</p></p>

                            <p>Time: 9:00 PM Onwards</p>
                        </div>
                      </div>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-color:purple; opacity:0.5;" ></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                   <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>--%>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-color:purple; opacity:0.5;" ></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>--%>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-color:purple; opacity:0.5;"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>--%>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-color:purple; opacity:0.5;" ></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>--%>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-color:purple; opacity:0.5;"  ></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                   <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
            </div>
          </div>
        </div>
      </div>
   
    <!--friday events end-->
   

   <!--sat Events start-->
   <div class="b-gallery">
        <div data-bgimage="" class="b-gallery__img-wrap bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
        <div class="container">
          <div class="row">
            <div class="b-item__header col-xs-12">
              <h6 class="s6-heading b-item__ttl-header"id="Saturday">Saturday</h6>
            </div>
            <div id="sliderGallery" class="b-gallery__slider btn-white slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Previous" role="button" aria-disabled="true" style="display: block;">Previous</button>
              <div aria-live="polite" class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 3516px; left: 0px;">
                  <article class="c-box c-box--sm slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style=" background-color:purple; opacity:0.5; background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">June</span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>--%>
  <div id="headingIconClrtwo" role="tab" class="b-accordion__panel-heading">
                        <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" href="#blockIconClrtwo" aria-expanded="true" aria-controls="blockIconClrtwo" style="color:white;"><b class="fa fa-music"style="color:white;"></b> <B>DJ NIGHT </B><b class="fa fa-angle-down"></b></a></h4>
                      </div>
                      <div id="blockIconClrtwo" role="tabpanel" aria-labelledby="headingIconClrtwo" class="b-accordion__collapse collapse in">
                        <div class="b-accordion__body">
                          <p>DJ: DJ Ankytrixx</p>

                           <p>Period: 23<sup>rd</sup> June, 2018</p>

                            <p>Time: 9:00 PM Onwards</p>
                           
                         
                        </div>
                      </div>

 
              </article>
                  <article class="c-box c-box--sm slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style=" background-color:purple; opacity:0.5; background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">30</span><span class="date-sm__rh"><span class="date-sm__rh-m">June</span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>--%>
 <div id="headingIconClrtwo" role="tab" class="b-accordion__panel-heading">
                        <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" href="#blockIconClrtwo" aria-expanded="true" aria-controls="blockIconClrtwo" style="color:white;"><b class="fa fa-music"style="color:white;"></b> <B>DJ NIGHT </B><b class="fa fa-angle-down"></b></a></h4>
                      </div>
                      <div id="blockIconClrtwo" role="tabpanel" aria-labelledby="headingIconClrtwo" class="b-accordion__collapse collapse in">
                        <div class="b-accordion__body">
                          <p>DJ: DJ Huassain </p>

                           <p>Period: 30<sup>th</sup> June, 2018</p>

                            <p>Time: 9:00 PM Onwards</p>
                           
                         
                        </div>
                      </div>
              </article>
                  <article class="c-box c-box--sm slick-slide slick-active" data-slick-index="2" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-color:purple;opacity:0.5;   background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                   <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>--%>
              </article>
                  <article class="c-box c-box--sm slick-slide slick-active" data-slick-index="3" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style=" background-color:purple;opacity:0.5;    background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>--%>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="4" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="5" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_05_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="6" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="7" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="8" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="9" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="10" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="11" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>

                                                                   </div>

              </div>
              
              
               <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;" aria-disabled="false">Next</button></div>
          </div>
        </div>
      </div>

    <!--sat events end-->
   



    <!--sun events starts-->
    <div class="b-gallery">
        <div data-bgimage="" class="b-gallery__img-wrap bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
        <div class="container">
          <div class="row">
            <div class="b-item__header col-xs-12">
              <h6 class="s6-heading b-item__ttl-header"id="Sunday">Sunday</h6>
            </div>
            <div id="sliderGallery" class="b-gallery__slider btn-white slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Previous" role="button" aria-disabled="true" style="display: block;">Previous</button>
              <div aria-live="polite" class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 3516px; left: 0px;">
                  <article class="c-box c-box--sm slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style=" background-color:purple; background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;); opacity:0.5"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>--%>
              </article>
                  <article class="c-box c-box--sm slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style=" background-color:purple;background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);opacity:0.5"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>--%>
              </article>
                  <article class="c-box c-box--sm slick-slide slick-active" data-slick-index="2" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style="  background-color:purple; background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);opacity:0.5"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>--%>
              </article>
                  <article class="c-box c-box--sm slick-slide slick-active" data-slick-index="3" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" style="  background-color:purple; background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;); opacity:0.5"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d">2018</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <%--<h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>--%>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="4" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="5" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_05_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="6" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="7" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="8" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="9" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="10" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="11" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article></div></div>
              
              
              <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;" aria-disabled="false">Next</button></div>
          </div>
        </div>
      </div>
    <!--sun ends here-->
    
    
     <!--other events starts-->
    <div class="b-gallery">
        <div data-bgimage="" class="b-gallery__img-wrap bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
        <div class="container">
          <div class="row">
            <div class="b-item__header col-xs-12">
              <h6 class="s6-heading b-item__ttl-header"id="Others">Others</h6>
            </div>
           <!-- <div id="sliderGallery" class="b-gallery__slider btn-white slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Previous" role="button" aria-disabled="true" style="display: block;">Previous</button>
              <div aria-live="polite" class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 3516px; left: 0px;"><article class="c-box c-box--sm slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="images/ot1.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="images/ot2.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide slick-active" data-slick-index="2" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="images/ot4.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide slick-active" data-slick-index="3" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="images/oth3.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="4" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="5" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_05_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="6" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="7" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="8" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="9" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="10" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="11" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article></div></div>
              
              
              <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;" aria-disabled="false">Next</button></div>-->
          </div>
        </div>
      </div>
    <!--other ends here-->



    <!--event high-->

    <div class="b-promo" style="background-color:black; border-top:solid;border-color:#EE3124;">
        <div class="container">
          <h2 class="b-promo__s2-ttl" style="color:goldenrod;">‘EVENTS @ HIGH!'</h2>
          
           <div class="col-md-3">
			</div>
            
             <div class ="col-md-6">

                 <div class="col-md-12">
                       <div class="form-box">
                          <input type="text" name="name" placeholder="First Name" required style="width:100%;height: 35px;" >
                       </div>
                 </div>
                 <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                          <input type="text" name="name" placeholder="Email" required style="width:100%;height: 35px;" >
                       </div>
                 </div>
                  <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                          <input type="text" name="name" placeholder="Mobile" required style="width:100%;height: 35px;" >
                       </div>
                 </div>
                  <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                          <input type="datetime-local" name="name" placeholder="Date" required style="width:100%;height: 35px;" >
                       </div>
                 </div>  
                 <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                         <select style="width:100%;height: 35px;">
                          <option value="volvo">No of People</option>
                          <option value="volvo">1</option>
                          <option value="saab">2</option>
                          <option value="mercedes">3</option>
                          <option value="audi">4</option>
                          <option value="audi">5</option>
                        </select>
                       </div>
                 </div> 
                 <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                         <select style="width:100%;height: 35px;">
                          <option value="volvo">Type of Events</option>
                          <option value="volvo">Social</option>
                          <option value="saab">Corporate</option>
                          <option value="mercedes">Other Celebrations</option>
                         
                        </select>
                       </div>
                 </div>  
                  <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                     <button class="b-form__btn indent btn-bg--act-b">SUBMIT</button>
<%--                               <b style="color:white; background-color:goldenrod;font-size: 18px;">Submit</b></button>--%>
                       </div>
                 </div>                       
             </div>
             <div class ="col-md-3">
                                            
            </div>
          
        </div>
      </div>
    
<%-- <br />
    <br />--%>
    
    <!-- event high-->


</asp:Content>

