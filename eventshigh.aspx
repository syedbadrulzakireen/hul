<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="eventshigh.aspx.cs" Inherits="eventshigh" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style type="text/css">
         @font-face {
         font-family: "Univers LT W01_57 Condensed";
         src: url("/Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix");
         src: url("/Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix") format("eot"),url("/Fonts/1476004/bf8f8741-5f64-4db9-a877-a44624092e68.woff2") format("woff2"),url("/Fonts/1476004/7ce02c2c-45d4-4dee-90f2-f8034e29ac24.woff") format("woff"),url("/Fonts/1476004/0955c906-88fc-47e8-8ea2-0765bdf88050.ttf") format("truetype");
         }
      </style>
     <style>
          @media screen and (max-width: 960px) {
            .container {
                width: 100%;
            }
 
            #a1 {
                width: 20%;
            }
        }
          @media screen and (max-width:640px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                margin-right:10px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:100%!important;
                 margin-right:10px!important;
            }
             .mobimMargin{
             
                 margin-top:150px!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
                  margin-right:10px!important;
            }
            .mobimstext{
 
                padding-left:50px!important;
            }
             .mobimMargin{
             
                 margin-top:150px!important;
                width:100% !important;
            }
               .date-lg{
             padding: 26px 6px!important;
         }
        }
           @media screen and (max-width:760px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:100%!important;
                 margin-right:10px!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
            }
             .mobimstext{
 
                padding-left:50px!important;
            }
            .mobimMargin{
             
                 margin-top:150px!important;
                width:100% !important;
            }
              .date-lg{
             padding: 26px 6px!important;
         }
        }
           @media screen and (max-width:960px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                padding:0px!important
            }
 
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:100%!important;
                 margin-right:10px!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
            }
            .mobimstext{
 
                padding-left:50px!important;
            }
            .mobimMargin{
             
                 margin-top:150px!important;
                width:100% !important;
            }
              .date-lg{
             padding: 26px 6px!important;
         }
             .feedback  {
                 top:100px!important;
             }
          
        }
         @media screen and (max-width:1200px) {
           .mobimMargin{
             
                 margin-top:150px!important;
                width:100% !important;
            }
             .date-lg{
             padding: 26px 6px!important;
         }
         }
         .date-lg__rh-m{
             width:80px!important;
         }
         .date-lg__ad {
         display:block;}
         
    .b-panel-t::before {
    border-left: 0px solid #4d4d4d !important;
}
    .b-panel-t {
    background-color: #4d4d4d !important;
    padding: 0px 40px 0px 25px !important; 
}
     </style>
    <style>
table, th, td {
    border: 8px solid white;
}
</style>
     <div class="b-events" style="background-image:url(images/ImagesUpdated/super-high13.jpg);background-attachment:fixed;background-size:cover!important ">
 
   <%-- <div data-stellar-background-ratio="1.2" data-bgimage="images/ImagesUpdated/super-high13.jpg" class="c-headlines__img-wrap parallax bg-image"></div> --%>       
    <div class="container">          
        <%--<div class="row mobimMargin">
             <div class="container"> 
                <div class="col-md-12   ">
 
                   <div class="col-md-3"></div>
                    <div class="col-md-3 col-xs-12 mobimMargin ">--%>
                        <%--<div class="c-headlines__date-lg">
                            <time datetime="2015-09-23T08:00" class="date-lg" style="width:260px;">
                                <span class="date-lg__dt" style="font-size:18px;">10/17/24</span>
                                <span class="date-lg__rh">
                                    <span class="date-lg__rh-m" ></span>
                                    <span class="date-lg__rh-m">Aug 2018</span>
                                    <span class="date-lg__rh-t"></span>
                                </span>
                                <span class="date-lg__ad" ><a href="#Friday">Fri</a></span>
                            </time>
                        </div>--%>

<%--                        <TABLE BORDER=6 CELLSPACING=0 CELLPADDING=10 style="width:236px"> 

<TR> 
<TD style="color:white!important; font-size:21px;">--%><%--<p style="color:white!important;">10/17/24--%><%--<center><p id="demo " style="color:black!important;"></p>10/17/24/31</center>--%>
<%--<script> document.write(new Date().toLocaleDateString()); </script>--%>
<%--</p>
</TD>
<TD style="color:white!important; font-size:21px;">
    Aug 2018--%>
<%--<p id="demo" style="color:white!important;">Aug 2018</p>--%>

<%--<script>
var d = new Date(2018,8);
document.getElementById("demo").innerHTML = d;
</script>--%><%-- </TD></TR>--%>
                           

<%--
<TR> 
<TD COLSPAN=3 ALIGN=CENTER style="color:white!important; font-size:21px;">

    <a href="#friday" style="    font-size: 35px;"  >Friday</a>--%>

<%--<p id="demo " style="color:black!important;"></p>--%>

<%--<script>

    var d = new Date();
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    var n = weekday[d.getDay()];
    document.getElementById("demo").innerHTML = n;

</script>--%>
<%--</TD></TR>



</TABLE> 
</CENTER> 
                    </div>
                        <div class="col-md-3 col-xs-12 mobimMargin ">--%>
                        <%--<div class="c-headlines__date-lg">
                            <time datetime="2015-09-23T08:00" class="date-lg" style="width:260px;">
                                <span class="date-lg__dt" style="font-size:18px;">10/17/24</span>
                                <span class="date-lg__rh">
                                    <span class="date-lg__rh-m" ></span>
                                    <span class="date-lg__rh-m">Aug 2018</span>
                                    <span class="date-lg__rh-t"></span>
                                </span>
                                <span class="date-lg__ad" ><a href="#Friday">Fri</a></span>
                            </time>
                        </div>--%>

                  <%--      <TABLE BORDER=6 CELLSPACING=0 CELLPADDING=10 style="width:236px"> 

<TR> 
<TD style="color:white!important; font-size:21px;"><%--<p style="color:white!important;">10/17/24--%><%--<center><p id="demo " style="color:black!important;"></p>04/11/18/25</center>--%>
<%--<script> document.write(new Date().toLocaleDateString()); </script>--%>
<%--</p>
</TD>
<TD style="color:white!important; font-size:21px;">
    Aug 2018--%>
<%--<p id="demo" style="color:white!important;">Aug 2018</p>--%>

<%--<script>
var d = new Date(2018,8);
document.getElementById("demo").innerHTML = d;
</script>--%><%-- </TD></TR>--%>
                           


<%--<TR> 
<TD COLSPAN=3 ALIGN=CENTER style="color:white!important; font-size:21px;">

    <a href="#saturday" style="    font-size: 35px;"  >Saturday</a>--%>

<%--<p id="demo " style="color:black!important;"></p>--%>

<%--<script>

    var d = new Date();
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    var n = weekday[d.getDay()];
    document.getElementById("demo").innerHTML = n;

</script>--%>
<%--</TD></TR>



</TABLE> 
</CENTER> 
                    </div>
                    
                    <div class="col-md-3"></div>
                 </div> 
             </div>
        </div>--%>
 
        <div class="row">
             <div class="col-md-12">
                    <div class="b-content container">
                        <div class="b-panel-t col-xs-12 feedback " style="top: 0px;margin-bottom: -100px; opacity:0.8;border-left: 0px solid #4d4d4d !important;" >
                            <div class="b-panel-t__list col-md-12 col-md-3 ">
                                <p  style="color:white;text-align:inherit;">
                                    <p style="color:white; font-size: 25px;text-align:justify;font-family:'Univers LT W01_57 Condensed' !important; ">I went on a week day. I experienced it to be really awesome experience but it wasn't though it was really nice. Food, service and ambience were good. It's worth visiting and can go multiple times as well.</p>
                                    <p style="color:white; font-size: 25px; text-align:right;font-family:'Univers LT W01_57 Condensed' !important; ">Review in Zomato By Pradeep. </p>
                                </p>
                            </div>  
                        </div>
                    </div>
             </div>          
        </div>
       
        <div class="container" id="Thursday"></div>
 
        <div class="row"  style="margin-top:150px!important">
 
                <div class="b-item__header col-xs-12">
                    <h6 class="s6-heading b-item__ttl-header" style="color:white; font-size:25px; font-family:'Univers LT W01_57 Condensed' !important;">Holi Special</h6>
                </div>
                
                <div class="col-md-12">
                    <%--row 2 --%>  
                    <!--===========================   DJ Thursday 1 start   =====================-->
                    <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images/DJ/High.png); background-size:cover!important ">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">10</span><span class="date-sm__rh"><span class="date-sm__rh-m">March</span><span class="date-sm__rh-d">2020</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                          <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" rel="nofollow" class="b-box__link" target="_blank"></a>
                        </div>
 
                        <div id="headingIconClrthu" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrOne" aria-expanded="true" aria-controls="blockIconClrOne" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>High Mae Holi ft NJay, Devuja, & Ritu Malhotra </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrthu" role="tabpanel" aria-labelledby="headingIconClrthu" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body"  style="color:white">
                                <p style="color:white">
                                 Artist:> -NJay, Devuja, & Ritu Malhotra</p>
                                <p>Date: 10<sup>th</sup> mar,2020 </p>
                                <p>Time: 11:00 AM Onwards</p>
                            </div>
                        </div>
                         
                
                    </article>
                    </div> 
    <!--===========================   DJ Thursday 1 ends   =====================-->


                    <%--   <!--===========================   DJ Thursday 2 start   =====================-->
                   <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images/DJ/rinton.jpg); background-size:cover!important ">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">19</span><span class="date-sm__rh"><span class="date-sm__rh-m">April</span><span class="date-sm__rh-d">2019</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" rel="nofollow" class="b-box__link" target="_blank"></a>
                        </div>
 
                        <div id="headingIconClrthree" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrthree" aria-expanded="true" aria-controls="blockIconClrthree" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>DJ Night</B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrthree" role="tabpanel" aria-labelledby="headingIconClrthree" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                Artist:  DJ RINTON</p>
                                <p>Date: 19<sup>th</sup> Apr, 2019</p>
                                <p>Time: 9:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>
                    </div>
    <!--===========================   DJ Thursday 2 ends   =====================-->
--%>


<%--                       <!--===========================   DJ Thursday 3    start   =====================-->
                     <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images); background-size:cover!important " >
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">26</span><span class="date-sm__rh"><span class="date-sm__rh-m">April</span><span class="date-sm__rh-d">2019</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="headingIconClrtwo" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrtwo" aria-expanded="true" aria-controls="blockIconClrtwo" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>DJ Night </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrtwo" role="tabpanel" aria-labelledby="headingIconClrtwo" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                Artist: DJ Blaque</p>
                                <p>Date: 26<sup>th</sup> Apr, 2019</p>
                                <p>Time: 9:00 PM Onwards</p>
                            </div>
                        </div>
                                        
                    </article>
                    </div>
                       <!--===========================   DJ  Thursday   3 ends   =====================-->

--%>

    <!--===========================   DJ Thursday  4 start   =====================-->
                 <%--   <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images/DecemberDj/Silvr2.png); background-size:cover!important " style="border:1px solid #e15e32; background-color:purple; opacity:0.7">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">Nov</span><span class="date-sm__rh-d">2018</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="headingIconClrfour" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrfour" aria-expanded="true" aria-controls="blockIconClrfour" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>DJ Night </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrfour" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                Artist: DJ Silvr</p>
                                <p>Date: 25<sup>th</sup> Jan, 2019</p>
                                <p>Time: 9:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>
                    </div>--%>
                       <!--===========================   DJ Thursday 4 ends   =====================-->
                    <%--row 2 end  --%> 
                 </div>
        </div>
        
        <div class="container" id="friday"></div>
 
        <div class="row"  style="margin-top:150px!important">
 
<%--                <div class="b-item__header col-xs-12">
                    <h6 class="s6-heading b-item__ttl-header" style="color:white; font-size:25px; font-family:'Univers LT W01_57 Condensed' !important;"><%--Friday</h6>
                </div>--%>
                
                <div class="col-md-12">
                    <%--row 2 --%>  
                    <!--===========================   DJ Friday 1 start   =====================-->
            <%--  <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images/DJ/CandyShop.png); background-size:cover!important ">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">21</span><span class="date-sm__rh"><span class="date-sm__rh-m">feb</span><span class="date-sm__rh-d">2020</span></span></time>
                            </div>
 
                          <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                 <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" rel="nofollow" class="b-box__link" target="_blank"></a>
                        </div>
 
                        <div id="headingIconClrOne" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrOne" aria-expanded="true" aria-controls="blockIconClrOne" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>Candy Shop ft. DJ Nash</B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrOne" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body"  style="color:white">
                                <p style="color:white">
                                 Artist: DJ Nash</p>
                                <p>Date: 21<sup>th</sup> feb,2020 </p>
                                <p>Time: 08:00 PM Onwards</p>
                            </div>
                        </div>
                         
                
                    </article>
                    </div> --%>
    <!--===========================   DJ Friday 1 ends   =====================-->


                       <!--===========================   DJ Friday 2 start   =====================-->
                  <%-- <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images//DJ/Snapchat.jpeg); background-size:cover!important ">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">25</span><span class="date-sm__rh"><span class="date-sm__rh-m">Oct</span><span class="date-sm__rh-d">2019</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" rel="nofollow" class="b-box__link" target="_blank"></a>
                        </div>
 
                        <div id="headingIconClrthree" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrthree" aria-expanded="true" aria-controls="blockIconClrthree" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>Snapchat party</B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrthree" role="tabpanel" aria-labelledby="headingIconClrthree" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                Artist:  DJ Silvr-Snapchat party</p>
                                <p>Date: 25<sup>th</sup> October, 2019</p>
                                <p>Time: 9:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>
                    </div>--%>
    <!--===========================   DJ Friday 2 ends   =====================-->



                       <!--===========================   DJ Friday 3    start   =====================-->
                <%--     <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images/DJ/Denuja.jpeg); background-size:cover!important " >
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">17</span><span class="date-sm__rh"><span class="date-sm__rh-m">Jan</span><span class="date-sm__rh-d">2020</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="headingIconClrtwo" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrtwo" aria-expanded="true" aria-controls="blockIconClrtwo" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>All Black Night </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrtwo" role="tabpanel" aria-labelledby="headingIconClrtwo" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                Artist:  DJ  Devuja</p>
                                <p>Date: 17<sup>th</sup> Jan, 2020</p>
                                <p>Time: 8:00 PM Onwards</p>
                            </div>
                        </div>
                                        
                    </article>
                    </div>--%>
                       <!--===========================   DJ  Friday   3 ends   =====================-->



    <!--===========================   DJ Friday  4 start   =====================-->
               <%--     <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images/Dj/perchdj.jpeg); background-size:cover!important " style="border:1px solid #e15e32; background-color:purple; opacity:0.7">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">28</span><span class="date-sm__rh"><span class="date-sm__rh-m">Dec</span><span class="date-sm__rh-d">2019</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="headingIconClrfour" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrfour" aria-expanded="true" aria-controls="blockIconClrfour" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>Alice In Wonderland with DJ Perch </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrfour" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                Artist: DJ Perch</p>
                                <p>Date: 28<sup>th</sup> Dec, 2019</p>
                                <p>Time: 8:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>
                    </div>--%>
                       <!--===========================   DJ Friday 4 ends   =====================-->
                    <%--row 2 end  --%> 
                 </div>
        </div>
    
       <%--<div class="container" ></div><div class="container" ></div>--%>
        <div class="row" id="saturday"></div>
        <div class="row" style="margin-top:50px!important">
 
                <div class="b-item__header col-xs-12">
                    <h6 class="s6-heading b-item__ttl-header"   style="color:white; font-size: 25px; font-family:'Univers LT W01_57 Condensed' !important;">Saturday</h6>
                </div>
                
                <div class="col-md-12">

                       <!--===========================   DJ SATURDAY 1 start   =====================-->
                  <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images/Dj/Overdose1.jpeg); background-size:cover!important " style="border:1px solid #e15e32; background-color:purple; opacity:0.7">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">07</span><span class="date-sm__rh"><span class="date-sm__rh-m">Mar</span><span class="date-sm__rh-d">2020</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="headingIconClrfour" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrfour" aria-expanded="true" aria-controls="blockIconClrfour" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>Overdose ft. NJay </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrfour" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                Artist: DJ NJay</p>
                                <p>Date: 07<sup>th</sup> Mar, 2020</p>
                                <p>Time: 8:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>
                    </div>
                      <!--===========================   DJ SATURDAY 1 ends   =====================-->
                      <!--===========================   DJ SATURDAY 2 start   =====================-->

             <%--<div class="col-md-3">  
                    <article class="c-box c-box--sm"><img src="" />
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images/Dj/stellernursery.jpeg); background-size:cover!important ">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">29</span><span class="date-sm__rh"><span class="date-sm__rh-m">feb</span><span class="date-sm__rh-d">2020</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" rel="nofollow" class="b-box__link" target="_blank"></a>
                        </div>
 
                        <div id="headingIconClrtwo" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrsix" aria-expanded="true" aria-controls="blockIconClrtwo" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>Overdose: The Techno Edition ft. Stellar Nursery</B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrsix" role="tabpanel" aria-labelledby="headingIconClrtwo" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body"  style="color:white">
                                <p style="color:white">
                                 Artist:Dj Stellar Nursery</p>
                                <p>29<sup>th</sup> feb, 2020 </p>
                                <p>Time: 08:00 PM Onwards</p>
                            </div>
                        </div>
                         
                
                    </article>
                    </div>--%>
                      <!--===========================   DJ SATURDAY 2 ends  =====================-->
                      <!--===========================   DJ SATURDAY 3 start   =====================-->
                    <%--  <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-image:url(images/Dj/Hussain18.jpeg); background-size:cover!important ">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">18</span><span class="date-sm__rh"><span class="date-sm__rh-m">January</span><span class="date-sm__rh-d">2019</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" rel="nofollow" class="b-box__link" target="_blank"></a>
                        </div>
 
                        <div id="headingIconClrtwo" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrseven" aria-expanded="true" aria-controls="blockIconClrtwo" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>High on Bollywood</B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrseven" role="tabpanel" aria-labelledby="headingIconClrtwo" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body"  style="color:white">
                                <p style="color:white">
                                 Artist:  DJ Hussain</p>
                                <p>18<sup>th</sup> Jan, 2020 </p>
                                <p>Time: 8:00 PM Onwards</p>
                            </div>
                        </div>
                         
                
                    </article>
                    </div>--%>

                      <!--===========================   DJ SATURDAY 3ends   =====================-->


                 <!--===========================   DJ SATURDAY 4 start   =====================-->
 
                    <%-- <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner"  style="border:1px solid #e15e32; background-image:url(images/DJ/Hassan1.jpg); background-size:cover!important "tyle="border:1px solid #e15e32; background-color:purple; opacity:0.7">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">26</span><span class="date-sm__rh"><span class="date-sm__rh-m">Jan</span><span class="date-sm__rh-d">2019</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="Div6" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClreight" aria-expanded="true" aria-controls="blockIconClreight" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>DJ Night </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClreight" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                               Artist: DJ Hassan</p>
                                <p>Date: 26<sup>th</sup> Jan, 2019</p>
                                <p>Time: 9:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>

                    </div>--%>
                      <!--===========================   DJ SATURDAY 4 ends   =====================-->
                    <%--row 2 end  --%> 
                 </div>
        </div>          
   <!--===========================   DJ Sunday EVENTS  start   =====================-->
        <div class="row"  style="margin-top:50px!important">
 
              <%--  <div class="b-item__header col-xs-12">
                    <h6 class="s6-heading b-item__ttl-header" style="color:white;font-family:'Univers LT W01_57 Condensed' !important;  font-size: 25px;">Sunday</h6>
                </div>--%>
                
                <div class="col-md-12">
                    <%--row 2 --%>  
                  <%-- <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32;background-image:url(images/DJ/a26.jpg); background-size:cover!important">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">Sept</span><span class="date-sm__rh-d">2018</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="headingIconClrOne" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrOne" aria-expanded="true" aria-controls="blockIconClrOne" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>Sundowner </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                       <div id="blockIconClrOne" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                Artist: A-26</p>
                                <p>Date: 23<sup>rd</sup> Sep, 2018</p>
                                <p>Time: 5:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>
                    </div> 
 --%>
                  <%--   <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-color:#4d4d4d; opacity:0.7">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d"></span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="headingIconClrOne" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrOne" aria-expanded="true" aria-controls="blockIconClrOne" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>Sundowner</B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrOne" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                DJ: DJ Nash</p>
                                <p>Period:07<sup>th</sup> July, 2018</p>
                                <p>Time: 9:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>
                    </div>--%>
 
                   <%--  <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32; background-color:#4d4d4d; opacity:0.7">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt"></span><span class="date-sm__rh"><span class="date-sm__rh-m"></span><span class="date-sm__rh-d"></span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="headingIconClrOne" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrOne" aria-expanded="true" aria-controls="blockIconClrOne" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>Sundowner </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrOne" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                DJ: DJ Nash</p>
                                <p>Period:07<sup>th</sup> July, 2018</p>
                                <p>Time: 5:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>
                    </div>--%>
 
                 <%--    <div class="col-md-3">  
                    <article class="c-box c-box--sm">
                        <div class="c-box__inner" style="border:1px solid #e15e32;background-image:url(images/DJ/Band.jpg); background-size:cover!important /*background-color:#4d4d4d; opacity:0.7*/">
 
                            <div data-bgimage="" class="b-box__img-wrap b-box__grdnt-b bg-image" ></div>
 
                            <div class="c-box__date-item c-box__date-item--float"  >
                                <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">28</span><span class="date-sm__rh"><span class="date-sm__rh-m">Oct</span><span class="date-sm__rh-d">2018</span></span></time>
                            </div>
 
                            <ul class="c-box__views">
                                <li>
                                    <button class="b-form__btn indent btn-bg--act-b" style="min-height:30px;font-family:'Univers LT W01_57 Condensed' !important;">BOOK</button>
                                </li>
                             </ul>
                             <a href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book" target="_blank" rel="nofollow" class="b-box__link"></a>
                        </div>
 
                        <div id="headingIconClrOne" role="tab" class="b-accordion__panel-heading">
                            <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" 
                                href="#blockIconClrnine" aria-expanded="true" aria-controls="blockIconClrOne" style="color:white;">
                                <b class="fa fa-music" style="color:white;"></b> 
                                <B>Sundowner </B>
                                <b class="fa fa-angle-down"></b></a>
                            </h4>
                         </div>
 
                        <div id="blockIconClrnine" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                            <div class="b-accordion__body" style="color:white">
                                <p style="color:white">
                                Artist: Best Kept Secret </p>
                                <p>Date: 28<sup>th</sup> Oct, 2018</p>
                                <p>Time: 5:00 PM Onwards</p>
                            </div>
                        </div>
                    </article>
                    </div>--%>
                    <%--row 2 end  --%> 
                 </div>
        </div>
          <!--===========================   DJ Sunday EVENTS ENDS    =====================-->
 
               <div class="row">
            <div class ="col-md-12">
                <center >
                    <h2 class="b-promo__s2-ttl" style="color:white;font-family:'Univers LT W01_57 Condensed' !important;">EVENTS @ HIGH</h2> 
                </center>
                <br />
                <div class="col-md-3"></div>
 
                <div class ="col-md-6">
 
                     <div class="col-md-12">
                           <div class="form-box">
                            <%--  <input type="text" name="name" placeholder="First Name" required style="width:100%;height: 35px; color:black" >--%>
                                <asp:TextBox ID="TextBox1" placeholder="Name" runat="server" style="width:100%;height: 40px;"></asp:TextBox>
                           </div>
                     </div>
                     <br />
                     <br />
                     <br />
                     <br />
                      <div class="col-md-12">
                           <div class="form-box">
                             <%-- <input type="text" name="name" placeholder="Email" required style="width:100%;height: 35px; color:black" >--%>
                                <asp:TextBox ID="TextBox2" placeholder="Email" runat="server" style="width:100%;height: 40px;"></asp:TextBox>
                           </div>
                     </div>
                      <br />
                     <br />
                     <br />
                     <br />
                      <div class="col-md-12">
                           <div class="form-box">
                              <%--<input type="text" name="name" placeholder="Mobile" required style="width:100%;height: 35px;color:black" >--%>
                                <asp:TextBox ID="TextBox3" placeholder="Mobile" runat="server" style="width:100%;height: 40px;"></asp:TextBox>
                           </div>
                     </div>
                      <br />
                     <br />
                     <br />
                     <br />
                      <div class="col-md-12">
                           <div class="form-box" style="color:black">
                              <%--<input type="datetime-local" name="name" placeholder="Date" required style="width:100%;height: 35px;color:black" >--%>
                               <asp:TextBox type="datetime-local" name="name" placeholder="Date" ID="TextBox4" runat="server" style="width:100%;height: 35px;color:black"></asp:TextBox>
                           </div>
                     </div>  
                     <br />
                     <br />
                     <br />
                     <br />
                      <div class="col-md-12">
                           <div class="form-box">
                             <%--<select style="width:100%;height: 35px; color:black">
                              <option value="volvo">No of People</option>
                              <option value="volvo">1</option>
                              <option value="saab">2</option>
                              <option value="mercedes">3</option>
                              <option value="audi">4</option>
                              <option value="audi">5</option>
                            </select>--%>
                          <asp:DropDownList ID="Members" runat="server" style="width:100%;height: 35px; color:black">
                              <asp:ListItem Enabled="true" Text="Number Of people" Value="-1"></asp:ListItem>
                              <asp:ListItem Text="One" Value="1"></asp:ListItem>
                              <asp:ListItem Text="Two" Value="2"></asp:ListItem>
   
                               <asp:ListItem Text="Three" Value="3"></asp:ListItem>
                               <asp:ListItem Text="Four" Value="4"></asp:ListItem>
                               <asp:ListItem Text="Five" Value="5"></asp:ListItem>

                      </asp:DropDownList>
                           </div>
                     </div> 
                     <br />
                     <br />
                     <br />
                     <br />
                      <div class="col-md-12">
                           <div class="form-box" style="color:black">
                             <asp:DropDownList ID="EventType" runat="server" style="width:100%;height: 35px; color:black">
    <asp:ListItem Enabled="true" Text="Type Of Event" Value="-1"></asp:ListItem>
    <asp:ListItem Text="Social" Value="Social"></asp:ListItem>
    <asp:ListItem Text="Corporate" Value="Corporate"></asp:ListItem>
   
    <asp:ListItem Text="Other Celebrations" Value="Other Celebrations"></asp:ListItem>
                               
</asp:DropDownList>
                           </div>
                     </div>  
                      <br />
                     <br />
                     <br />
                     <br />
               <div class="form-group col-md-12">
                                <asp:Image ID="imgCapthca" runat="server" ImageUrl="CaptchaImages.aspx" />
                                <br /><br />
                                <asp:TextBox ID="txtcaptchaText" runat="server" Width="100px" autocomplete="off"></asp:TextBox>  
                                <br /><br />
                                <asp:Label id="lblCaptcha" runat="server" Visible="false"></asp:Label>
                                <br /><br />
                                     </div>

                      <div class="col-md-12 col-sm-12 col-xs-12"> 
                           <div class="form-box col-xs-12" style="margin-left:30%">
                        <%-- <button class="b-form__btn indent btn-bg--act-b" style="align-content:center">SUBMIT</button>--%>
                                <asp:Button ID="Button1" runat="server" Text="Submit"   class="b-form__btn indent btn-bg--act-b" style="align-content:center" OnClick="Button1_Click"  />
    <%--                               <b style="color:white; background-color:goldenrod;font-size: 18px;">Submit</b></button>--%>
                           </div>
                     </div> 
                      
                                
                </div>
 
                <div class="col-md-3" id="Button1"></div>
 
            </div>
        </div>
      
    </div>
</div>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
<script src="Scripts/calendar-en.min.js" type="text/javascript"></script>
<link href="Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function () {
        $("#<%=TextBox4.ClientID %>").dynDateTime({
            showsTime: true,
            ifFormat: "%Y/%m/%d %H:%M",
            daFormat: "%l;%M %p, %e %m, %Y",
            align: "BR",
            electric: false,
            singleClick: false,
            displayArea: ".siblings('.dtcDisplayArea')",
            button: ".next()"
        });
    });
</script>
</asp:Content>
 
