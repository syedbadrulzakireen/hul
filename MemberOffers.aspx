<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="MemberOffers.aspx.cs" Inherits="MemberOffers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
      var MTUserId = '8d5e1514-dd0b-46fe-8111-004f45677b51';
      var MTFontIds = new Array();
      
      MTFontIds.push("1476004"); // Univers® W01 57 Condensed 
      (function () {
          var mtTracking = document.createElement('script');
          mtTracking.type = 'text/javascript';
          mtTracking.async = 'true';
          mtTracking.src = 'mtiFontTrackingCode.js';
      
          (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(mtTracking);
      })();
   </script>
   
   <style type="text/css">
      @font-face{ 
      font-family:"Univers LT W01_57 Condensed";
      src:url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix");
      src:url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix") format("eot"),url("Fonts/1476004/bf8f8741-5f64-4db9-a877-a44624092e68.woff2") format("woff2"),url("Fonts/1476004/7ce02c2c-45d4-4dee-90f2-f8034e29ac24.woff") format("woff"),url("Fonts/1476004/0955c906-88fc-47e8-8ea2-0765bdf88050.ttf") format("truetype");
      }
         
       </style>
    
    <style type="text/css">
    @font-face{
        font-family:"Univers LT W01_57 Condensed";
        src:url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix");
        src:url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix") format("eot"),url("Fonts/1476004/bf8f8741-5f64-4db9-a877-a44624092e68.woff2") format("woff2"),url("Fonts/1476004/7ce02c2c-45d4-4dee-90f2-f8034e29ac24.woff") format("woff"),url("Fonts/1476004/0955c906-88fc-47e8-8ea2-0765bdf88050.ttf") format("truetype");
    }
   
          @media screen and (max-width: 960px) {
            .container {
                width: 100%;
            }

            #a1 {
                width: 20%;
            }
        }
          @media screen and (max-width:640px) {
            .mobim{
                width:95%!important;
                margin:8px!important;
                margin-right:10px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:100%!important;
                 margin-right:10px!important;
                  background-repeat:no-repeat!important;
            }
             .shiny {
                width:100%!important;
                  height: auto;

            }
            .ctext{
                width:100% !important;
                 height:auto!important;
                  /*margin-right:10px!important;*/
                   font-size:20px;
            }
             .shiny {
                width:100%!important
            }
            .mobimstext{
 padding-left:50px!important;
                padding-right:50px!important;
            }
        }
           @media screen and (max-width:760px) {
            .mobim{
                width:95%!important;
               margin: 8px !important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:100%!important;
                 margin-right:10px!important;
                  background-repeat:no-repeat!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
                  font-size:20px;
                     margin-top: 138px;
            }
             .mobimstext{
 padding-left:50px!important;
                padding-right:50px!important;
            }
        }
           @media screen and (max-width:960px) {
            .mobim{
                width:95%!important;
                margin:8px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:100%!important;
                 margin-right:10px!important;
                 background-repeat:no-repeat!important;
            }
             .shiny {
                width:100%!important;
                  height: auto;

            }
            .ctext{
                width:100% !important;
                 height:auto!important;
                  font-size:20px;
            }
            .mobimstext{

                padding-left:50px!important;
                padding-right:50px!important;
            }
        }
    .b-panel-t::before {
    border-left: 0px solid #530d85;
}
        @media screen and (max-width:360px) {
            .mobim {
                width: 95% !important;
               margin: 8px !important;
                padding: 0px !important;
            }
            .shiny {
                width:100%!important;
                  height: auto;

            }

            .ctext {
                width: 100% !important;
                height: auto !important;
                font-size:18px;
                margin-top: 138px;
            }
        }


        .contents {
  width: 100%;
  position: relative;
  text-align: center;
}

h1 {
  text-align: center;
  margin: 2em 0;
}

.shiny {
  margin: 30px auto;
  /*background-color: #ffffff;*/
  border-radius: 15px;
  width: 520px;
  height: 290px;
  /*padding: 15px;*/
  overflow:hidden;
}

.show-off {
  width: 405px;
  height: 605px;
  position: relative;
  top: -556px;
  left: -550px;
  transition: 2s;
  transform: rotate(30deg);
  background: linear-gradient(90deg, rgba(255,255,255, 0)50%, rgba(255,255,255,0.7)100%);
}

.shiny:hover .show-off {
  transform: rotate(0);
  left: 560px;
  top: -445px;
}

    </style>
    
    <div class="b-headlines"style="background-image:url(images/high-View.jpg); background-attachment: fixed; background-size:cover!important;height:800px!important">
      <%--<div class="b-events" style="background-image: url(images/ImagesUpdated/Helipad.jpg); background-attachment: fixed; background-size:cover!important">--%>
      <%--  <div class="container" >
         </div>--%>
      <!-- Sliding end -->
      <!-- Current Offers 1-->
      <div class="row1 mobim ctext" style="color:azure!important; margin-top:100px" ></div>
         <h1  class=  " word-break ctext col-xs-12" style="color:White!important; text-align:center!important;margin-top:85px!important " > <a href="/Account/Login.aspx" class=" ctext col-xs-12" style="color:White!important; text-align:center!important;" > REGISTER NOW TO KNOW MORE </a></h1>
           <%--<p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";>
                                             <!--Collapsive-->                       
                                          <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem"><center>
                                                     <img src="images/Membership1.png" class="mobim" width="500" height="300" / style="border-radius:5%;"></center>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </p>    
      </div>--%>
  
  <div class="shiny ">    
    <img src="images/Membership1.png" class=" mobim" width="520" height="300" style="border-radius:5%;" alt="image" />
    <div class="show-off" />
  </div>
  

      <%--<div class="row">
         <a href="/Account/Login.aspx">
            <div class='scene'>
               <div class='sphere'>
                  <div class='ring' id='ring1'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring2'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring3'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring4'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring5'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring6'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring7'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring8'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring9'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring10'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring11'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
                  <div class='ring' id='ring12'>
                     <div class='spoke' id='spoke1'></div>
                     <div class='spoke' id='spoke2'></div>
                     <div class='spoke' id='spoke3'></div>
                     <div class='spoke' id='spoke4'></div>
                     <div class='spoke' id='spoke5'></div>
                     <div class='spoke' id='spoke6'></div>
                     <div class='spoke' id='spoke7'></div>
                     <div class='spoke' id='spoke8'></div>
                     <div class='spoke' id='spoke9'></div>
                     <div class='spoke' id='spoke10'></div>
                     <div class='spoke' id='spoke11'></div>
                     <div class='spoke' id='spoke12'></div>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>--%>
   <!-- Current Offers 1--></div>
  <%--  @-webkit-keyframes example {
    from {background-color: red;}
    to {background-color: yellow;}
}--%>

<%--/* Standard syntax */--%>
<%--@keyframes example {
    from {background-color: red;}
    to {background-color: yellow;}
}--%>
</asp:Content>