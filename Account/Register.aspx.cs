﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Hul_login.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Hul_login.Account
{

    public partial class Register : Page

    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            String Fname = FirstName.Value;
            String Mname = MiddleName.Value;
            String DOB = Dob.Value;
            String DOA = Doa.Value;
            String ResAddrs = Residential_Address.Value;
            String PhoneNo = Telephone_Number.Value;
            String Occupation1 = Occupation.Value;
            String Organiz = Organization.Value;
            String Organizadd = Organization_ADD.Value;
            String Salutation1 = Salute.SelectedValue;
            String MobNO = Mobile_Number.Value;
            String Lname = LastName.Value;



            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser() { UserName = Email.Text, Email = Email.Text, FirstName = Fname, MiddleName = Mname, DateOfBirth = DOB, DateOfAnniversary = DOA, ResidentialAddress = ResAddrs, Occupation = Occupation1, Organization = Organiz, OrganizationAddress = Organizadd,
            Salutation=Salutation1, LastName = Lname, MobileNumber = MobNO};
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                string s = roleManager.FindByName("User").Id;
                context.Database.ExecuteSqlCommand("insert into AspNetUserRoles (userid,Roleid) values({0},{1})", user.Id, s);

              
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                //string code = manager.GenerateEmailConfirmationToken(user.Id);
                //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

                signInManager.SignIn( user, isPersistent: false, rememberBrowser: false);
                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            else 
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }











        }
    }
}