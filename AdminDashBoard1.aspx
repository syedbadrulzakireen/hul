﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminDashBoard1.aspx.cs" Inherits="Hul_login.AdminDashBoard1" %>

<script runat="server">

    protected void BtnExcel_Click(object sender, EventArgs e)
    {

    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input{
            color:black;
            max-width:120px;
        }
        th{
           min-width:100px;
        }
        body{
            background-color:white;
            
        }
        label{
            color:black;
        }
        a{
            color:black;
        }
       
        
    </style>
   
    
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   

     <div class="b-events"style="background-color:white;background-attachment:fixed;background-size:100% 100%;padding-bottom:100%;">
    
        
           <div class="row" style="margin-left:10%;margin-top:2%">
               <div class="col-md-1 col-sm-2 col-lg-1">
                    <label>Sort by:</label>
            
                </div>

               <div class="col-md-4 col-sm-8 col-lg-4">
                   <asp:dropdownlist ID="SortList1"
              runat="server">
                <asp:ListItem>Salutation</asp:ListItem>
                <asp:ListItem>FirstName</asp:ListItem>
                <asp:ListItem>MiddleName</asp:ListItem>
                <asp:ListItem>LastName</asp:ListItem>
                <asp:ListItem>MobileNumber</asp:ListItem>
                <asp:ListItem>PhoneNumber</asp:ListItem>
                <asp:ListItem>UserName</asp:ListItem>
                <asp:ListItem>DateOfAnniversary</asp:ListItem>
                <asp:ListItem>ResidentialAddress</asp:ListItem>
                <asp:ListItem>Occupation</asp:ListItem>
                <asp:ListItem>Organization</asp:ListItem>
                <asp:ListItem>OrganizationAddress</asp:ListItem>
            </asp:dropdownlist>
                </div>
               </div>
         <div class="row" style="margin-left:10%;margin-top:5%;">
               <div class="col-md-1 col-sm-2 col-lg-1">
                    <label>Sort Order:</label>
            
                </div>

               <div class="col-md-4 col-sm-8 col-lg-4">
                  <asp:radiobuttonlist id="DirectionList"
              runat="server">
              <asp:listitem selected="true">Ascending</asp:listitem>
              <asp:listitem>Descending</asp:listitem>
            </asp:radiobuttonlist>
                </div>
               </div>
          <div class="row" style="margin-left:15%;">
                <asp:button id="SortButton" text="Sort" OnClick="SortButton_Click" runat="Server"/> 
        </div>
          <br/>
      <hr/>
      <br/>



         <div class="container"  style="font-family:'Univers LT W01_57 Condensed' !important;color:black;width:100%" >
            <div class="row">
                     <asp:GridView ID="GridView1" CssClass="table-responsive table table-condensed" HeaderStyle-CssClass="form-control-static" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" DataKeyNames="Id">
            <Columns>
                <asp:CommandField ShowEditButton="True" ControlStyle-Width="70px">
<ControlStyle Width="70px"></ControlStyle>
                </asp:CommandField>
                <asp:BoundField DataField="Salutation" HeaderText="Salutation" SortExpression="Salutation" />
                <asp:BoundField DataField="FIrstName" HeaderText="FIrstName" SortExpression="FIrstName" />
                <asp:BoundField DataField="MiddleName" HeaderText="MiddleName" SortExpression="MiddleName" />
                <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                <asp:BoundField DataField="DateOfBirth" HeaderText="DateOfBirth" SortExpression="DateOfBirth" />
                <asp:BoundField DataField="MobileNumber" HeaderText="MobileNumber" SortExpression="MobileNumber" />
                <asp:BoundField DataField="PhoneNumber" HeaderText="PhoneNumber" SortExpression="PhoneNumber" />
                <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
                <asp:BoundField DataField="DateOfAnniversary" HeaderText="DateOfAnniversary" SortExpression="DateOfAnniversary" />
                <asp:BoundField DataField="ResidentialAddress" HeaderText="ResidentialAddress" SortExpression="ResidentialAddress" />
                <asp:BoundField DataField="Occupation" HeaderText="Occupation" SortExpression="Occupation" />
                <asp:BoundField DataField="Organization" HeaderText="Organization" SortExpression="Organization" />
                <asp:BoundField DataField="OrganizationAddress" HeaderText="OrganizationAddress" SortExpression="OrganizationAddress" />
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" ReadOnly="True" SortExpression="Id" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HulConnectionString %>" SelectCommand="SELECT [Salutation], [FIrstName], [MiddleName], [LastName], [DateOfBirth], [MobileNumber], [PhoneNumber], [UserName], [DateOfAnniversary], [ResidentialAddress], [Occupation], [Organization], [OrganizationAddress], [Id] FROM [AspNetUsers]" ConflictDetection="CompareAllValues" DeleteCommand="DELETE FROM [AspNetUsers] WHERE [Id] = @original_Id AND (([Salutation] = @original_Salutation) OR ([Salutation] IS NULL AND @original_Salutation IS NULL)) AND (([FIrstName] = @original_FIrstName) OR ([FIrstName] IS NULL AND @original_FIrstName IS NULL)) AND (([MiddleName] = @original_MiddleName) OR ([MiddleName] IS NULL AND @original_MiddleName IS NULL)) AND (([LastName] = @original_LastName) OR ([LastName] IS NULL AND @original_LastName IS NULL)) AND (([DateOfBirth] = @original_DateOfBirth) OR ([DateOfBirth] IS NULL AND @original_DateOfBirth IS NULL)) AND (([MobileNumber] = @original_MobileNumber) OR ([MobileNumber] IS NULL AND @original_MobileNumber IS NULL)) AND (([PhoneNumber] = @original_PhoneNumber) OR ([PhoneNumber] IS NULL AND @original_PhoneNumber IS NULL)) AND [UserName] = @original_UserName AND (([DateOfAnniversary] = @original_DateOfAnniversary) OR ([DateOfAnniversary] IS NULL AND @original_DateOfAnniversary IS NULL)) AND (([ResidentialAddress] = @original_ResidentialAddress) OR ([ResidentialAddress] IS NULL AND @original_ResidentialAddress IS NULL)) AND (([Occupation] = @original_Occupation) OR ([Occupation] IS NULL AND @original_Occupation IS NULL)) AND (([Organization] = @original_Organization) OR ([Organization] IS NULL AND @original_Organization IS NULL)) AND (([OrganizationAddress] = @original_OrganizationAddress) OR ([OrganizationAddress] IS NULL AND @original_OrganizationAddress IS NULL))" InsertCommand="INSERT INTO [AspNetUsers] ([Salutation], [FIrstName], [MiddleName], [LastName], [DateOfBirth], [MobileNumber], [PhoneNumber], [UserName], [DateOfAnniversary], [ResidentialAddress], [Occupation], [Organization], [OrganizationAddress], [Id]) VALUES (@Salutation, @FIrstName, @MiddleName, @LastName, @DateOfBirth, @MobileNumber, @PhoneNumber, @UserName, @DateOfAnniversary, @ResidentialAddress, @Occupation, @Organization, @OrganizationAddress, @Id)" OldValuesParameterFormatString="original_{0}" UpdateCommand="UPDATE [AspNetUsers] SET [Salutation] = @Salutation, [FIrstName] = @FIrstName, [MiddleName] = @MiddleName, [LastName] = @LastName, [DateOfBirth] = @DateOfBirth, [MobileNumber] = @MobileNumber, [PhoneNumber] = @PhoneNumber, [UserName] = @UserName, [DateOfAnniversary] = @DateOfAnniversary, [ResidentialAddress] = @ResidentialAddress, [Occupation] = @Occupation, [Organization] = @Organization, [OrganizationAddress] = @OrganizationAddress WHERE [Id] = @original_Id AND (([Salutation] = @original_Salutation) OR ([Salutation] IS NULL AND @original_Salutation IS NULL)) AND (([FIrstName] = @original_FIrstName) OR ([FIrstName] IS NULL AND @original_FIrstName IS NULL)) AND (([MiddleName] = @original_MiddleName) OR ([MiddleName] IS NULL AND @original_MiddleName IS NULL)) AND (([LastName] = @original_LastName) OR ([LastName] IS NULL AND @original_LastName IS NULL)) AND (([DateOfBirth] = @original_DateOfBirth) OR ([DateOfBirth] IS NULL AND @original_DateOfBirth IS NULL)) AND (([MobileNumber] = @original_MobileNumber) OR ([MobileNumber] IS NULL AND @original_MobileNumber IS NULL)) AND (([PhoneNumber] = @original_PhoneNumber) OR ([PhoneNumber] IS NULL AND @original_PhoneNumber IS NULL)) AND [UserName] = @original_UserName AND (([DateOfAnniversary] = @original_DateOfAnniversary) OR ([DateOfAnniversary] IS NULL AND @original_DateOfAnniversary IS NULL)) AND (([ResidentialAddress] = @original_ResidentialAddress) OR ([ResidentialAddress] IS NULL AND @original_ResidentialAddress IS NULL)) AND (([Occupation] = @original_Occupation) OR ([Occupation] IS NULL AND @original_Occupation IS NULL)) AND (([Organization] = @original_Organization) OR ([Organization] IS NULL AND @original_Organization IS NULL)) AND (([OrganizationAddress] = @original_OrganizationAddress) OR ([OrganizationAddress] IS NULL AND @original_OrganizationAddress IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_Id" Type="String" />
                <asp:Parameter Name="original_Salutation" Type="String" />
                <asp:Parameter Name="original_FIrstName" Type="String" />
                <asp:Parameter Name="original_MiddleName" Type="String" />
                <asp:Parameter Name="original_LastName" Type="String" />
                <asp:Parameter Name="original_DateOfBirth" Type="String" />
                <asp:Parameter Name="original_MobileNumber" Type="String" />
                <asp:Parameter Name="original_PhoneNumber" Type="String" />
                <asp:Parameter Name="original_UserName" Type="String" />
                <asp:Parameter Name="original_DateOfAnniversary" Type="String" />
                <asp:Parameter Name="original_ResidentialAddress" Type="String" />
                <asp:Parameter Name="original_Occupation" Type="String" />
                <asp:Parameter Name="original_Organization" Type="String" />
                <asp:Parameter Name="original_OrganizationAddress" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Salutation" Type="String" />
                <asp:Parameter Name="FIrstName" Type="String" />
                <asp:Parameter Name="MiddleName" Type="String" />
                <asp:Parameter Name="LastName" Type="String" />
                <asp:Parameter Name="DateOfBirth" Type="String" />
                <asp:Parameter Name="MobileNumber" Type="String" />
                <asp:Parameter Name="PhoneNumber" Type="String" />
                <asp:Parameter Name="UserName" Type="String" />
                <asp:Parameter Name="DateOfAnniversary" Type="String" />
                <asp:Parameter Name="ResidentialAddress" Type="String" />
                <asp:Parameter Name="Occupation" Type="String" />
                <asp:Parameter Name="Organization" Type="String" />
                <asp:Parameter Name="OrganizationAddress" Type="String" />
                <asp:Parameter Name="Id" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Salutation" Type="String" />
                <asp:Parameter Name="FIrstName" Type="String" />
                <asp:Parameter Name="MiddleName" Type="String" />
                <asp:Parameter Name="LastName" Type="String" />
                <asp:Parameter Name="DateOfBirth" Type="String" />
                <asp:Parameter Name="MobileNumber" Type="String" />
                <asp:Parameter Name="PhoneNumber" Type="String" />
                <asp:Parameter Name="UserName" Type="String" />
                <asp:Parameter Name="DateOfAnniversary" Type="String" />
                <asp:Parameter Name="ResidentialAddress" Type="String" />
                <asp:Parameter Name="Occupation" Type="String" />
                <asp:Parameter Name="Organization" Type="String" />
                <asp:Parameter Name="OrganizationAddress" Type="String" />
                <asp:Parameter Name="original_Id" Type="String" />
                <asp:Parameter Name="original_Salutation" Type="String" />
                <asp:Parameter Name="original_FIrstName" Type="String" />
                <asp:Parameter Name="original_MiddleName" Type="String" />
                <asp:Parameter Name="original_LastName" Type="String" />
                <asp:Parameter Name="original_DateOfBirth" Type="String" />
                <asp:Parameter Name="original_MobileNumber" Type="String" />
                <asp:Parameter Name="original_PhoneNumber" Type="String" />
                <asp:Parameter Name="original_UserName" Type="String" />
                <asp:Parameter Name="original_DateOfAnniversary" Type="String" />
                <asp:Parameter Name="original_ResidentialAddress" Type="String" />
                <asp:Parameter Name="original_Occupation" Type="String" />
                <asp:Parameter Name="original_Organization" Type="String" />
                <asp:Parameter Name="original_OrganizationAddress" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
        
        
            </div>

        </div>
         <div style="margin-top:5%;margin-left:20%">
         <asp:Button runat="server" ID="BtnExcel" Text="Export to Excel" OnClick="BtnExcel_Click1" />

        </div>
            </div>
    
        
</asp:Content>
