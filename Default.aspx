<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Hul_login._Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        h1 {
            font-family: 'Univers LT W01_57 Condensed' !important;
            font-size: 10vw;
            width: 100vw;
            margin-top: calc(50vh - 10vw);
            text-align: center;
            background: linear-gradient( 160deg, hsl(0, 75%, 50%) 10%, hsl(0, 0%, 70%) 10%, hsl(0, 0%, 70%) 25%, hsl(0, 75%, 50%) 25%, hsl(0, 75%, 50%) 40%, hsl(0, 0%, 70%) 40%, hsl(0, 0%, 70%) 55%, hsl(0, 75%, 50%) 55%, hsl(0, 75%, 50%) 70%, hsl(0, 0%, 70%) 70%, hsl(0, 0%, 70%) 85%, hsl(0, 75%, 50%) 85% );
            /*text-shadow: 0.5px -0.6vw #fff4;*/
            color: #fff;
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            animation: 10s BeProud linear infinite, 3s Always ease alternate infinite;
        }

        @keyframes BeProud {
            100% {
                background-position: 100vw 0px;
            }
        }

        @keyframes Always {
            100% {
                transform: scale(1.1);
            }
        }


        .mybackground {
            background-attachment: fixed;
            background-size: cover;
            background-repeat: no-repeat;
            height: 100%;
            width: 100%;
        }




        #content {
            padding: 0;
            border: 0px solid black;
            width: auto;
            height: 400px;
        }

        .galleryItem {
            border: 0px solid black;
            height: 100px;
            width: 351px;
            margin-left: 10px;
            clear: both;
            -webkit-transition: line-height 1s, height 1s, opacity 1s, border-radius 3s;
        }

            .galleryItem:hover {
                background-color: #202020;
                color: #fff;
                height: 233px;
                line-height: 50px;
                display: block;
                border-radius: 20px;
                opacity: 0.8;
            }

            .galleryItem p {
                display: none;
                margin-top: 2px !important;
                font-size: 13px;
            }

            .galleryItem:hover p {
                display: block;
            }

        .box {
            position: relative;
            top: -90px;
            padding: 5px 15px 7px 20px;
        }

        .title {
            font-weight: bold;
            color: #EE3124;
            /*font-size: 87px;*/
            margin-top: -105px;
        }

        @media screen and (max-width: 600px) {
            .title {
                color: #EE3124;
                font-weight: bold;
                font-size: 59px;
                margin-top: 110px;
            }

            .mySlides {
                display: none;
                border-radius: 10px;
            }

            .c-box__inner {
                width: 100% !important;
            }

            .b-panel-t {
                width: 100% !important;
                margin-top: 150px;
                line-height: 20px;
                height: auto !important;
            }

            .txtfix {
                line-height: 40px !important;
                margin-right: 10px;
            }

            .txtfix1 {
                margin-top: 60px !important;
            }
        }
    </style>
    <script type="text/javascript" src="/Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var body = $('body');
            var backgrounds = [
                'url(slider1.jpg)',
                'url(slider2.jpg)',
                'url(slider3.jpg)',
                'url(slider4.jpg)',
                'url(slider5.jpg)',
                'url(slider6.jpg)'];
            var current = 0;

            function nextBackground() {
                body.css(
                    'background-image',
                    backgrounds[current = ++current % backgrounds.length]);

                setTimeout(nextBackground, 5000);

            }
            setTimeout(nextBackground, 5000);
            body.css('background-image', backgrounds[0]);
            $("body").addClass("mybackground");


        });
    </script>

   

    <div class="main-slide ">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
      <div class="main-slide__btn-item " style="opacity: 0.1;"></div>
      <div id="sliderMain" class="main-slide__slider ">
         <div class="main-slide__item-a "> <div class="txtfix1" style="margin-top:-70px">
                     
                  <h1 style="font-size:80px;">HIGH Ultra Lounge</h1></div>
          
            <div class="row">
               <div class="">
                  <div class="">
                     
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="row">
                  <div class="b-tabs col-lg-8" style="width:99%;">
                     <div class="b-content container ">
                        <div class="row">
                           <div class="b-panel-t box col-xs-12 col-lg-2 col-md-2 col-sm-3 col-xm-12" style="top: 22px;   padding-top:20px; width:270px; height:80px; /*margin-bottom: -32px;    opacity: 0.7;    width: 20%;    height: 10px;   padding-bottom:28px;padding-top:20px; margin-left: -1.3%;*/ border-left: 0px solid #800080;" >
                              <b style="color:white; text-align:justify;font-family:'Univers LT W01_57 Condensed' !important; padding: 8px 15px 6px 15px !important; font-size: 24px; padding:0px!important;   /*padding-left:8%*/  ">FEATURED UPCOMING  EVENTS</b>
                              <%-- <div class="b-panel-t__list col-md-12 col-xs-3 ">
                                 <p  style="color:white;text-align:inherit;"> </p>
                                 
                                         </div>--%>
                           </div>
                        </div>
                     </div>
                     <%--<h6 class="s6-heading b-tabs__ttl" style=" font-family:'Univers LT W01_57 Condensed' !important";>Featured Events</h6>--%>
                     <ul role="" class="">
                        <li role="presentation" class="active">
                           <%--<a href="#block-a" aria-controls="block-a" role="tab" data-toggle="tab">--%>
                           <%-- <svg baseProfile="tiny" width="10" height="10" x="0px" y="0px" viewBox="0 0 10 10" xml:space="preserve">
                              <path fill="#231F20" d="M0,4h4V0H0V4z M6,0v4h4V0H6z M0,10h4V6H0V10z M6,10h4V6H6V10z"/>
                              </svg>--%>
                       <%--    </a>--%>
                        </li>
                        <li role="presentation">
                           <a href="#block-b" aria-controls="block-b" role="tab" data-toggle="tab">
                           <%-- <svg baseProfile="tiny" width="10" height="10" x="0px" y="0px" viewBox="0 0 10 10" xml:space="preserve">
                              <path fill="#231F20" d="M0,0v4h10V0H0z M10,10V6H0v4H10z"/>
                              </svg>--%>
                           </a>
                        </li>
                     </ul>
                     <div class="b-tabs__content">
                        <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                         <!-- DJ EVENTS -->
                         

                            <!---First week---------------->
                 <div class="row">
                 <%--   <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/overdose1.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Overdose Ft N.Jay</h3>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <p></p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-108-29T20:00" class="date c-box__date"><span class="date__dt">07</span><span class="date__rh"><span class="date__rh-m">Mar</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">8:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="#" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                 </div>
                              </div>
                           <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>--%>
                                     <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                   
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/Dj/High.png); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">  High Mae Holi ft NJay, Devuja, & Ritu Malhotra   </h3>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <p>Celebrate Holi 421 ft above ground Level! along with Unlimited Organic Colours|Top Celebrities DJs|Live Dhol in Bollywood ,Punjabi,commercial and in house Music forms and
 Enjoy the colourfull festival in open air  with loud and non stop Music. </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">10</span><span class="date__rh"><span class="date__rh-m">Mar</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">11:00 am</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="#" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                 </div>
                              </div>
                               
                          
                              
                           </div>

                            <!---Second week---------------->
                               <div class="row">s
                             
<%--                              <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/Denuja.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">All Black Night with DJ Devuja</h3>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <p>Kick start your weekend to the beats of   DJ Devuja  at HIGH Ultra Lounge.  DJ Devuja is all set to pump up this Friday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force.</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">17</span><span class="date__rh"><span class="date__rh-m">Jan</span><span class="date__rh-d" >Fri</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="#" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>                                    
                                 </div>
                              </div>
                              <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>
                       <%--         <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/Dj/Hussain18.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">High on Bollywood with DJ Hussain </h3>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                 <p>Hussain is back at HIGH Ultra Lounge with  latest Hip-Hop and EDM edition. The mega weekend is here and it�s getting bigger and better at the plush rooftop lounge. Allow DJ Rohit Barker  to entertain you with his groovy set of insane Hip Hop and EDM chartbusters. </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">18</span><span class="date__rh"><span class="date__rh-m">Jan</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="#" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                 </div>
                              </div>--%>
                           </div>
                            <!---Third  week---------------->
                           <div class="row">
                             
<%--                              <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">                             
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/High_Fridays_Valentine2.png); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Njay</h3>
                                     
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                 <div class="galleryItem">
                                                      About Event
                                                     <p>Catch Dj NJAY at HIGH Ultra Lounge this friday. Dj NJAY is all set to get you grooving and make your friday memorable.

Dance your heart out to the beats of House music and set the floor on fire!

</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">14</span><span class="date__rh"><span class="date__rh-m">Feb</span><span class="date__rh-d" >Fri</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                 
                                 </div>
                              </div>
                              <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>--%>
<%--                           <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                  
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/dj/CandyShop.png); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Candy Shop ft. DJ Nash</h3>
                                      
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                <div class="galleryItem">
                                                      About Event
                                                      <p>Kick start your weekend to the beats of the Resident DJ, DJ Silvr at HIGH Ultra Lounge. DJ Silvr is all set to pump up this Friday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force. </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">21</span><span class="date__rh"><span class="date__rh-m">feb</span><span class="date__rh-d" >fri</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>                                   
                                 </div>
                              </div>--%>
                           </div>
                            <!---Fourth week---------------->
                                     <div class="row">
                                         <%-- <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                  
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/dj/CandyShop.png); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Candy Shop ft. DJ Nash</h3>
                                      
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                <div class="galleryItem">
                                                      About Event
                                                      <p>Kick start your weekend to the beats of the Resident DJ, DJ Silvr at HIGH Ultra Lounge. DJ Silvr is all set to pump up this Friday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force. </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">21</span><span class="date__rh"><span class="date__rh-m">feb</span><span class="date__rh-d" >fri</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>                                   
                                 </div>
                              </div>
                                         <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>--%>
<%--                                           <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                  
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/calmChor.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important"> The Exchange: Pop Up & After Party ft. Calm Chor & Rohit Barker</h3>
                                      
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <p>Kick start your weekend to the beats of the Resident DJ, DJ Silvr at HIGH Ultra Lounge. DJ Silvr is all set to pump up this Friday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force. </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">22</span><span class="date__rh"><span class="date__rh-m">Feb</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>                                   
                                 </div>
                              </div>--%>
                              <%--<div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>--%>
                         <%--  <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                  
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/stellernursery.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Overdose: The Techno Edition ft. Stellar Nursery</h3>
                                      
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                              <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <p>Kick start your weekend to the beats of the Resident DJ, DJ Perch at HIGH Ultra Lounge. DJ Perch is all set to pump up this saturday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force. </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">29</span><span class="date__rh"><span class="date__rh-m">FEB</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>                                   
                                 </div>
                              </div>--%>
                           </div>
                               <!---Fith week---------------->
                                <div class="row">
<%--                                <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                  
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/stellernursery.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Overdose: The Techno Edition ft. Stellar Nursery</h3>
                                      
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                              <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <p>Kick start your weekend to the beats of the Resident DJ, DJ Perch at HIGH Ultra Lounge. DJ Perch is all set to pump up this saturday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force. </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">29</span><span class="date__rh"><span class="date__rh-m">FEB</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>                                   
                                 </div>
                              </div>                          --%>
                              <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>
                              <%--<div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">                             
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/neff.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Hip Hop Night DJ Nuthan & Neff</h3>
                                     
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                     <p>Kick start your weekend to the beats of the  DJ Nuthan & Neff at HIGH Ultra Lounge.  DJ Nuthan & Neff is all set to pump up this saturday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force.</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">Nov</span><span class="date__rh-d" >Thu</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                 
                                 </div>
                              </div>--%>
                             

                           </div>
                           
                            <!--Pastb events----> 

                                
                               <div class="row">
                             <div class="b-content container ">
                        <div class="row">
                           <div class="b-panel-t box col-xs-12 col-lg-2 col-md-2 col-sm-3 col-xm-12" style="top: 22px;   padding-top:20px; width:270px; height:60px; /*margin-bottom: -32px;    opacity: 0.7;    width: 20%;    height: 10px;   padding-bottom:28px;padding-top:20px; margin-left: -1.3%;*/ border-left: 0px solid #800080;" >
                              <b style="color:white; text-align:justify;font-family:'Univers LT W01_57 Condensed' !important; padding: 8px 15px 6px 15px !important; font-size: 24px; padding:0px!important;   /*padding-left:8%*/  ">PAST EVENTS</b>
                            
                           </div>
                        </div>
                     </div>
                                           <div class="row">
                                   <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/overdose1.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Overdose Ft N.Jay</h3>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <p></p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-108-29T20:00" class="date c-box__date"><span class="date__dt">07</span><span class="date__rh"><span class="date__rh-m">Mar</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">8:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="#" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                 </div>
                              </div>
                                   </div>


                                    <div class="row">
                                <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                  
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/stellernursery.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Overdose: The Techno Edition ft. Stellar Nursery</h3>
                                      
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                              <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <p>Kick start your weekend to the beats of the Resident DJ, DJ Perch at HIGH Ultra Lounge. DJ Perch is all set to pump up this saturday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force. </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">29</span><span class="date__rh"><span class="date__rh-m">FEB</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                     <%--  <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>--%>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>                                   
                                 </div>
                              </div>                          
                                       </div>
                                    <div class="row">
                                          <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                  
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/dj/CandyShop.png); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Candy Shop ft. DJ Nash</h3>
                                      
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                               <%-- <div class="galleryItem">
                                                      About Event
                                                      <p>Kick start your weekend to the beats of the Resident DJ, DJ Silvr at HIGH Ultra Lounge. DJ Silvr is all set to pump up this Friday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force. </p>
                                                   </div>--%>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">21</span><span class="date__rh"><span class="date__rh-m">feb</span><span class="date__rh-d" >fri</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                         <%-- <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>--%>
                                       </div>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>                                   
                                 </div>
                              </div>
                                         <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>
                                           <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                  
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/calmChor.jpeg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important"> The Exchange: Pop Up & After Party ft. Calm Chor & Rohit Barker</h3>
                                      
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <%--<div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <p>Kick start your weekend to the beats of the Resident DJ, DJ Silvr at HIGH Ultra Lounge. DJ Silvr is all set to pump up this Friday night as he brings you a night with foot-tapping music. Catch the DJ live at his console and get into your dancing shoes as the weekend starts in full force. </p>
                                                   </div>
                                                </div>--%>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">22</span><span class="date__rh"><span class="date__rh-m">Feb</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">08:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>                                   
                                 </div>
                              </div>
                             
                           </div>
                                 
                             
                                    
                            
                                    
      
                                      
                              
                                 
                                  

                            
                        </div>
                        <div   id="bottom_link" style="width: 100%" ><iframe width="100%" height="800" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=No.%2026%2F1%2C%20Rooftop%2C%20World%20Trade%20Center%2C%20Bangalore%20Brigade%20Gateway%20Campus%2C%20Dr.%20Rajkumar%20Road%2C%20Malleswaram%2C%20Bengaluru%2C%20Karnataka%20560055+(high%20ultra%20lounge)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Add map to website</a></iframe></div>
                        <br />

                     </div>
                  </div>
               </div>
            </div>


         </div>
        
         
         <!--end-->
      </div>
   </div>
   
</asp:Content >
