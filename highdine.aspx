﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeFile="highdine.aspx.cs" Inherits="highdine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Sliding start -->
    <div class="main-slide">
        <div class="main-slide__btn-item"></div>
        <div id="sliderMain" class="main-slide__slider">
          <div class="main-slide__item-a">
            <div data-bgimage="images/high-dine.jpg" class="main-slide__img-wrap bg-image"></div>
            <div class="container" >
              
            </div>
          </div>
        
          <div class="main-slide__item-b" >
             
            <div class="main-slide__bg-wrap bg-grdnt"  style="background-image:url(images/high-mix.jpg)"></div>
            <div class="container">
              
            </div>
          </div>
        </div>
      </div>
     <!-- Sliding end -->

    <!-- high dine-->

    <div class="b-promo" style="background-color:black; border-top:solid;border-color:darkgoldenrod;">
        <div class ="row">
        <div class="col-md-6">
          <h2 class="b-promo__s2-ttl" style="color:goldenrod;">‘HIGH DINE'</h2>
          <p style="color:white;text-align: justify;">
          The linear 10,000 sq. ft. of area is cleverly demarcated into 4 zones that can be easily integrated or
               separated from one another. The rationale behind dividing the spaces was that each would meet your
               mood and occasion. 'High Dine ', is a private smaller party area that is also open to sky with equally
               commanding views and, an exclusive enclosed area for memberships, and private events. The fluidity and
               lightness to the design that is easy on the eyes, a timeless quality to the materials and finishes, and
               a positive energy created by the various design elements; one that does justice to experiencing this 
              unique vantage point.
            
          </p>
          
        </div>
             <div class="col-md-6">
         
           <img src="images/high_dine_form.jpg" style="width: 100%;height: 315px;"/>
          <div class="btn-pointer-b__wrap">
            <a href="contacthigh.aspx" class="btn-pointer-b btn-anim-b"><b>Enquiry</b></a>
            <svg class="left" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>
            
            <svg class="right" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>
            
          </div>
        </div>
      </div>
    </div>

    <!--high dine-->

    <!-- google map -->
    <div class="b-promo" style="background-color:black; border-top:solid;border-color:darkgoldenrod;">
        <div style="width: 100%"><iframe width="100%" height="600" 
             src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=No.%2026%2F1%2C%20Rooftop%2C%20World%20Trade%20Center%2C%20Bangalore%20Brigade%20Gateway%20Campus%2C%20Dr.%20Rajkumar%20Road%2C%20Malleswaram%2C%20Bengaluru%2C%20Karnataka%20560055+(High%20Ultra%20Lounge)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" 
             frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">
                 Google Maps iframe generator</a></iframe>

        </div>
          
    </div> 
    <!-- google map -->
     
</asp:Content>
