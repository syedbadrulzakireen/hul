//using Limilabs.Client.SMTP;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eventshigh : System.Web.UI.Page
{
    private SmtpClient smtp;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            bool isCaptchavalid = false;

            if (Session["captchaText"] != null && Session["captchaText"].ToString() == txtcaptchaText.Text)
            {
                isCaptchavalid = true;
            }
            if (isCaptchavalid)
            {
                MailMessage mail = new MailMessage();
                mail.To.Add("high@sheraton.com");

                mail.From = new MailAddress("technical@shlrtechnosoft.in ", "Contact");
                mail.Subject = "Enquiry";
                mail.Body = "Name :" + TextBox1.Text + Environment.NewLine + Environment.NewLine +
                             "Contact Number :" + TextBox2.Text + Environment.NewLine + Environment.NewLine +
                              Environment.NewLine + Environment.NewLine +
                              "Email:" + TextBox3.Text + Environment.NewLine +
                             "Text Area:" + TextBox4.Text + Environment.NewLine + Environment.NewLine;

                mail.IsBodyHtml = false;

                smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Credentials = new System.Net.NetworkCredential("technical@shlrtechnosoft.in ", "Technical@123");

                smtp.Port = 587;
                smtp.EnableSsl = true;


                smtp.Send(mail);

                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Message Sent Successfully!');", true);

                lblCaptcha.Visible = true;
                lblCaptcha.Text = "Recaptcha Validation Success";
                lblCaptcha.ForeColor = Color.Green;
            }
            else
            {
                lblCaptcha.Visible = true;
                lblCaptcha.Text = "Recaptcha Validation Failed";
                lblCaptcha.ForeColor = Color.Red;
            }

            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
            txtcaptchaText.Text = "";
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);

        }
    }
}