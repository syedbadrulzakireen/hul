﻿using SRVTextToImage;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CaptchaImages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CaptchaRandomImage CI = new CaptchaRandomImage();
        string captchaText = CI.GetRandomString(5);
        Session["CaptchaText"] = captchaText;
        CI.GenerateImage(captchaText, 200, 50, System.Drawing.Color.DarkGray, System.Drawing.Color.White);
        this.Response.Clear();
        this.Response.ContentType = "image/jpeg";
        CI.Image.Save(this.Response.OutputStream, ImageFormat.Jpeg);
        CI.Dispose();
    }
}