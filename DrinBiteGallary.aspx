﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="DrinBiteGallary.aspx.cs" Inherits="DrinBiteGallary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <head> 
   <style>
	   
	   @media screen and (max-width:640px) {
            .slideshow-container{
                margin-left: 5px !important;
                margin-top: 216px !important;
                margin-right: 5px !important;
}
               
            }

* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 38px;
  transition: 1.5s ease;
  border-radius: 0 3px 3px 0;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
	    .dialog {
  /*background: #ddd;
  border: 1px solid #ccc;
  border-radius: 5px;
  float: left;*/
  /*height: 900px;
  margin: 20px;*/
 position: relative;
  /*width: 900px;*/
  
}

.close-thik:after {
  content: '✖'; /* UTF-8 symbol */
}
</style>
</head>
<body>

<div class="slideshow-container" style="margin-top:100px;">

<div class="mySlides   ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/f1.jpg" style="width:100%">
	<div class="text"></div></div>
</div>

<div class="mySlides">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c2.jpg" style="width:100%">
	<div class="text"></div></div>
</div>

<div class="mySlides ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c3.jpg" style="width:100%">
	<div class="text"></div></div>
</div>
    <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c4.jpg" style="width:100%">
		<div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c5.jpg" style="width:100%">
  <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c6.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c7.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>

     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c8.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c9.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c10.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c11.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c12.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c13.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/f14.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/f15.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="FOOD_BEVERAGES.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/food/FoodGallary/c16.jpg" style="width:100%">
		 <div class="text"></div></div>
</div>




<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>

</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
    <span class="dot" onclick="currentSlide(4)"></span> 
</div>
	<script>
//var slideIndex = 1;
//var slideIndex = getParameterByName('ImgIndex');
var slideIndex = window.location.search;
    slideIndex = slideIndex .split("?")[1].split("=")[1];
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>

</asp:Content>
