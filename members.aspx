<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="members.aspx.cs" Inherits="members" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
								
<script type="text/javascript">
    var MTUserId = '8d5e1514-dd0b-46fe-8111-004f45677b51';
    var MTFontIds = new Array();

    MTFontIds.push("1476004"); // Univers® W01 57 Condensed 
    (function () {
        var mtTracking = document.createElement('script');
        mtTracking.type = 'text/javascript';
        mtTracking.async = 'true';
        mtTracking.src = 'mtiFontTrackingCode.js';

        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(mtTracking);
    })();
</script>

<style type="text/css">
    @font-face{
        font-family:"Univers LT W01_57 Condensed";
        src:url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix");
        src:url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix") format("eot"),url("Fonts/1476004/bf8f8741-5f64-4db9-a877-a44624092e68.woff2") format("woff2"),url("Fonts/1476004/7ce02c2c-45d4-4dee-90f2-f8034e29ac24.woff") format("woff"),url("Fonts/1476004/0955c906-88fc-47e8-8ea2-0765bdf88050.ttf") format("truetype");
    }
   
          @media screen and (max-width: 960px) {
            .container {
                width: 100%;
            }

            #a1 {
                width: 20%;
            }
        }
          @media screen and (max-width:640px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                margin-right:10px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
               width: 90%!important;
                background-size:100%!important;
                 margin-left:5px!important;
                 background-repeat:no-repeat!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
                  margin-right:10px!important;
            }
            .mobimstext{
                 width:80% !important;
                padding-left:20px!important;
                padding-right:20px !important;
            }
        }
           @media screen and (max-width:760px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                padding:0px!important
            }
           .mobims{
                height:214px!important;
               width: 90%!important;
                background-size:100%!important;
                 margin-left:5px!important;
                 background-repeat:no-repeat!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
            }
             .mobimstext{
                 width:80% !important;
                padding-left:20px!important;
                padding-right:20px !important;
            }
        }
           @media screen and (max-width:960px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
               width: 90%!important;
                background-size:100%!important;
                 margin-left:5px!important;
                 background-repeat:no-repeat!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
            }
            .mobimstext{
                 width:80% !important;
                padding-left:20px!important;
                padding-right:20px !important;
            }
        }
    .b-panel-t::before {
    border-left: 0px solid #530d85;
}</style>
                             
								
                                
        <div class="b-headlines"style="background-image:url(images/high-mix.jpg);height:350px;">
        <div class="container" >
        </div>
      </div>
     <!-- Sliding end -->
    

    <!-- Current Offers 1-->
      
   <div class="b-events"style="background-color:black;padding-top:0px">>
        <div class="container">
          <div class="row">
            <div class="b-tabs col-lg-8 " style="width:108% ;">
              <%--<h6 class="s6-heading b-tabs__ttl"style="color:white;font-family:'Univers LT W01_57 Condensed' !important;">Members Offers:</h6>--%>
                       <div class="b-content container ">
         
        <div class="b-panel-t col-xs-4" style="top: 21px;    margin-bottom: -55px;    opacity: 0.9;    width: 255px;    height: 5px;    margin-left: -3%; border-left: 0px solid #530d85;" >
          <h2 style="color:white; text-align:justify;font-family:'Univers LT W01_57 Condensed' !important;     margin-top: -6px; "><b>Members Offers</b></h2>
        <div class="b-panel-t__list col-md-12 col-xs-3 ">
    <p  style="color:white;text-align:inherit;"> </p>

            </div>
       </div>
</div>           
                
                        <!--   <p class="c-box__txt"><br /><BR /><strong style="color:red;">Offer Valid Till JUNE 30TH SAT</strong></p>-->

              <ul role="tablist" class="b-tabs__nav b-tabs__nav-tabs">
                <li role="presentation" class="active"><a href="#block-a" aria-controls="block-a" role="tab" data-toggle="tab">
                    <svg baseProfile="tiny" width="10" height="10" x="0px" y="0px" viewBox="0 0 10 10" xml:space="preserve">
                    <path fill="#231F20" d="M0,4h4V0H0V4z M6,0v4h4V0H6z M0,10h4V6H0V10z M6,10h4V6H6V10z"/>
                    </svg></a></li>
                
              </ul>
              <div class="b-tabs__content">
                <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                  <div class="b-tabs__pane-item col-sm-6">
                  <%--  <div class="c-box">--%>
                      <div class="mobims" style="height:290px">
                        <div class="c-box__cont ">
                         <!-- <h3 class="c-box__ttl-s3">OFFER 1 </h3>
                          <hr class="hr-pattern">
                          <p class="c-box__txt"style="width:250px;">2 Vegetarian and 2 Non Vegetarian Appetizers</p><br />-->
                            <!--<p class="c-box__txt" style="width:250px">Offer Valid Till JUNE 30TH SAT </p><br />-->
                     <!--    <div class="col-xs-12 col-xm-6"><a href="#" class="btn indent btn-info btn-anim-a btn--act-a"style="width:220px;">
                          <span class="clr-black"style="height:50px;"></span>
                          <span><svg x="0px" y="0px"
                          viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                          <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           <b >Price-800&#8377</b></span></a></div>-->
<%--                          <p class="c-box__txt"style="width:350px;">NOTE:Cannot be clubbed with other offers. Prices does not include taxes.<br />Pick and choose appetizers from our menu</p>--%>

                        </div>
                          
                        <div data-bgimage="images/Membership1.png" class="b-box__img-wrap bg-image mobims" style="border-radius:3% ;width:526px;"></div>
                <!--        <div class="c-box__date-item">
<%--                            <h2 style="color:white;"><strong>VALIDITY</strong></h2>--%>
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">30</span><span class="date__rh"><span class="date__rh-m">June</span><span class="date__rh-d">Sat</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>-->
                          
                        <!-- <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Call Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>-->
                         <!--  <div class="c-box__link-wrap" style="right:380px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div>--><a href="#" rel="nofollow" class="b-box__link"></a>
                          <!--<div class="c-box__link-wrap"  style="right:2px">
                              <br/>
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contactdine.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           </div>
                        </div>--><a href="#" rel="nofollow" class="b-box__link"></a>
                      </div>
                  <%--  </div>--%>
                  </div>
                                      

               <div class="b-tabs__pane-item col-sm-6 ctext"  style="padding-left:25px">
                   <!-- <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">-->
                                       <h1 style="color:white; margin-top:0px;">&nbsp&nbsp&nbsp&nbsp  TERMS AND CONDITIONS</h1>
                        <ul> <li>  <h3 style="color:white;"> &#9679; Card entitles for free entry for self + 1</h3></li>
                             <li>   <h3 style="color:white;"> &#9679; Applicable on all days: except for entry on special events</h3></li>
                              <li><h3 style="color:white;"> &#9679; Card entitles for 10% discount on the total food & beverage bill &nbsp &nbsp &nbsp &nbsp &nbsp above INR 5,000 exclusive of taxes</h3></li>
                               <li><h3 style="color:white;"> &#9679; Right of admission reserved </h3></li>
                                <li><h3 style="color:white;">  &#9679; Card is non-transferrable</h3></li>
                              <li> <h3 style="color:white;font-family:univers "> &#9679; Management has the rights to retrieve the card benefits as well as &nbsp &nbsp membership anytime during the duration of the card's validity.</h3></li></ul>
                      
                          <!--<h3 class="c-box__ttl-s3">OFFER 2</h3>
                          <hr class="hr-pattern">-->
                       <!--   <p class="c-box__txt"style="width:250px;">3 Vegetarian and 3 Non vegetarian appetizers</p>-->
                            <br /><!--<p class="c-box__txt" style="width:250px">Offer Valid Till JUNE 30TH SAT </p><br />
                          <!--  <div class="col-xs-12 col-xm-6"><a href="#" class="btn indent btn-info btn-anim-a btn--act-a"style="width:220px;">
                          <span class="clr-black"style="height:50px;"></span>
                          <span><svg x="0px" y="0px"
                          viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                          <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                              
                           <b >Price-1200&#8377</b></span></a></div>-->
<%--                          <p class="c-box__txt"style="width:350px;">NOTE:Cannot be clubbed with other offers. Prices does not include taxes.<br />Pick and choose appetizers from our menu</p>--%>
                 
                        
                        <!--<div data-bgimage="images/Membership/membership2.jpg" class="b-box__img-wrap  bg-image"></div> -->
                    <!--    <div class="c-box__date-item">
<%--                              <h3 style="color:white;top:20px;"><strong>VALIDITY</strong></h3>--%>
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">30</span><span class="date__rh"><span class="date__rh-m">June</span><span class="date__rh-d">Sat</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>-->
                        
                     <!-- <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Call Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>-->


                        <!--  <div class="c-box__link-wrap" style="right:380px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>-->
                      <!-- </div><a href="#" rel="nofollow" class="b-box__link"></a>
                          <div class="c-box__link-wrap"  style="right:2px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>




                      </div>-->
                    </div>
                  </div>
               <div class="b-tabs__content">
                <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                  <div class="b-tabs__pane-item col-sm-6">
                  <%--  <div class="c-box">--%>
                      <div class=" mobims" style="height:290px">
                        <div class="c-box__cont">
                         <!-- <h3 class="c-box__ttl-s3">OFFER 1 </h3>
                          <hr class="hr-pattern">
                          <p class="c-box__txt"style="width:250px;">2 Vegetarian and 2 Non Vegetarian Appetizers</p><br />-->
                            <!--<p class="c-box__txt" style="width:250px">Offer Valid Till JUNE 30TH SAT </p><br />-->
                     <!--    <div class="col-xs-12 col-xm-6"><a href="#" class="btn indent btn-info btn-anim-a btn--act-a"style="width:220px;">
                          <span class="clr-black"style="height:50px;"></span>
                          <span><svg x="0px" y="0px"
                          viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                          <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           <b >Price-800&#8377</b></span></a></div>-->
<%--                          <p class="c-box__txt"style="width:350px;">NOTE:Cannot be clubbed with other offers. Prices does not include taxes.<br />Pick and choose appetizers from our menu</p>--%>

                        </div>
                          
                        <div data-bgimage="images/membership2.png" class="b-box__img-wrap bg-image mobims" style="border-radius:3%; width:526px;"></div>
                <!--        <div class="c-box__date-item">
<%--                            <h2 style="color:white;"><strong>VALIDITY</strong></h2>--%>
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">30</span><span class="date__rh"><span class="date__rh-m">June</span><span class="date__rh-d">Sat</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>-->
                          
                        <!-- <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Call Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>-->
                         <!--  <div class="c-box__link-wrap" style="right:380px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div>--><a href="#" rel="nofollow" class="b-box__link"></a>
                          <!--<div class="c-box__link-wrap"  style="right:2px">
                              <br/>
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contactdine.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           </div>
                        </div>--><a href="#" rel="nofollow" class="b-box__link"></a>
                      </div>
                   <%-- </div>--%>
                  </div>
                                      

                  <div class="b-tabs__pane-item col-sm-6 mobimstext" style="padding-left:0px; width:48%">
                   <!-- <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">-->
                                       <h1 style="color:white;font-family:'Univers LT W01_57 Condensed' !important; margin-top:5px; margin-left:30px">  TERMS AND CONDITIONS</h1>
                        <ul> <li>  <h3 style="color:white;font-family:'Univers LT W01_57 Condensed' !important;"> &#9679; Card entitles for free entry for lady member + 1</h3></li>
                             <li>   <h3 style="color:white;font-family:'Univers LT W01_57 Condensed' !important;text-align:justify"> &#9679; Applicable on all days: except for entry on special events</h3></li>
                              <li><h3 style="color:white;font-family:'Univers LT W01_57 Condensed' !important;">  &#9679; Card entitles for 10% discount on the total food & beverage bill &nbsp&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp&nbsp   above INR 5,000 exclusive of taxes</h3></li>
                               <li><h3 style="color:white;font-family:'Univers LT W01_57 Condensed' !important;"> &#9679; Right of admission reserved </h3></li>
                                <li><h3 style="color:white;font-family:'Univers LT W01_57 Condensed' !important;">  &#9679; Card is non-transferrable</h3></li>
                              <li> <h3 style="color:white;font-family:'Univers LT W01_57 Condensed' !important;">  &#9679; Management has the rights to retrieve the card benefits as well as  &nbsp &nbsp &nbsp &nbsp &nbsp   membership anytime during the duration of the card's validity.</h3></li></ul>
                    
                          <!--<h3 class="c-box__ttl-s3">OFFER 2</h3>
                          <hr class="hr-pattern">-->
                       <!--   <p class="c-box__txt"style="width:250px;">3 Vegetarian and 3 Non vegetarian appetizers</p>-->
                            <br /><!--<p class="c-box__txt" style="width:250px">Offer Valid Till JUNE 30TH SAT </p><br />
                          <!--  <div class="col-xs-12 col-xm-6"><a href="#" class="btn indent btn-info btn-anim-a btn--act-a"style="width:220px;">
                          <span class="clr-black"style="height:50px;"></span>
                          <span><svg x="0px" y="0px"
                          viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                          <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                              
                           <b >Price-1200&#8377</b></span></a></div>-->
<%--                          <p class="c-box__txt"style="width:350px;">NOTE:Cannot be clubbed with other offers. Prices does not include taxes.<br />Pick and choose appetizers from our menu</p>--%>
                 
                        </div>
                        <!--<div data-bgimage="images/Membership/membership2.jpg" class="b-box__img-wrap  bg-image"></div> -->
                    <!--    <div class="c-box__date-item">
<%--                              <h3 style="color:white;top:20px;"><strong>VALIDITY</strong></h3>--%>
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">30</span><span class="date__rh"><span class="date__rh-m">June</span><span class="date__rh-d">Sat</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>-->
                        
                     <!-- <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Call Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>-->


                        <!--  <div class="c-box__link-wrap" style="right:380px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>-->
                      <!-- </div><a href="#" rel="nofollow" class="b-box__link"></a>
                          <div class="c-box__link-wrap"  style="right:2px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>




                      </div>-->
                    </div>
                  </div>
                    </div>
              </div>
            </div>
          
          </div>
        </div>
     
    
    <!-- Current Offers 1-->
</asp:Content>

