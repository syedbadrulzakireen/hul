﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Hul_login.Startup))]
namespace Hul_login
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
