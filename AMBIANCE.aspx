<%--<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="AMBIANCE.aspx.cs" Inherits="AMBIANCE" %>--%>

<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="AMBIANCE.aspx.cs" Inherits="AMBIANCE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        @media screen and (max-width:640px) {
            .b-video-item {
                width: 300px !important;
            }
        }

        @media screen and (max-width:360px) {
            .b-video-item {
                width: 300px !important;
            }
        }
    </style>

    <div class="b-promo" style="background-color: black; border-top: solid; border-color: darkgoldenrod;">
        <div class="container">
            <h1 class="b-headlines__ttl" style="color: white; font-family: 'Univers LT W01_57 Condensed' !important; padding-top: 100px !important;">AMBIANCE</h1>


            <div class="container">
                <div class="row">
                    <%-- <div class="b-tabs col-lg-8" style="width: 99%">--%>
                    <%--<h6 class="s6-heading b-tabs__ttl">Current Offers</h6>--%>

                    <div class="b-tabs__content">
                        <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                            <div class="col-md-12">
                               <%-- <div class="b-tabs__pane-item col-sm-6">
                                   
                                    <video class="b-video-item" width="560" height="360" controls style="margin-top: 0!important;"> <source src="/images/videos/DJ%20JASMEET.mp4" type="video/mp4">
                    </video>

                                  
                                </div>--%>


                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Ambience14/50434652_1187518384735875_1490958073869631488_o.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="AmbianceGallary.aspx?ImgIndex=2" rel="nofollow" class="b-box__link" style="width: 600px!important; height: auto!important"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <h3 class="c-box__ttl-s3">High Dine</h3>
                                           <%-- <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Ambience14/High%20Dine.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="AmbianceGallary.aspx?ImgIndex=3" rel="nofollow" class="b-box__link" style="width: 600px!important; height: auto!important"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <h3 class="c-box__ttl-s3">High Edge</h3>
                                          <%--  <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Ambience14/High%20Edge.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="AmbianceGallary.aspx?ImgIndex=4" rel="nofollow" class="b-box__link" style="width: 600px!important; height: auto!important"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>


                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <h3 class="c-box__ttl-s3">High Mix</h3>
                                         <%--   <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Ambience14/High%20Mix.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="AmbianceGallary.aspx?ImgIndex=5" rel="nofollow" class="b-box__link" style="width: 600px!important; height: auto!important"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>


                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <h3 class="c-box__ttl-s3">High View</h3>
                                            <hr class="hr-pattern">
                                            <%--<p class="c-box__txt">High View</p>--%>
                                        </div>
                                        <div data-bgimage="images/Ambience14/High%20View-1.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="AmbianceGallary.aspx?ImgIndex=6" rel="nofollow" class="b-box__link" style="width: 600px!important; height: auto!important"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>


                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Ambience14/61497414_1283346955153017_4807223015987216384_o.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="AmbianceGallary.aspx?ImgIndex=7" rel="nofollow" class="b-box__link" style="width: 600px!important; height: auto!important"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Ambiance/Ambiance2.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="AmbianceGallary.aspx?ImgIndex=8" rel="nofollow" class="b-box__link" style="width: 600px!important; height: auto!important"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Ambiance/Ambiance3.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="AmbianceGallary.aspx?ImgIndex=9" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--  </div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Ambiance/Ambiance4.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="AmbianceGallary.aspx?ImgIndex=10" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>



                            </div>
                        </div>
                    </div>
                    <%-- </div>--%>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

